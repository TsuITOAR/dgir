#![feature(generic_const_exprs)]
#![feature(const_trait_impl)]
#![allow(incomplete_features)]
#![feature(effects)]
use dgir::{
    color::{Decorated, LayerData},
    cursor::{Assembler, CellCursor},
    draw::{curve::groups::Group, CircularArc, Resolution},
    shape::ArcCurve,
    units::{Angle, Length},
    zero, ToQuantity, MICROMETER,
};
use num::Zero;

mod common;

#[test]
fn pulley() {
    common::init();
    const RING_WIDTH: [Length<f64>; 2] = [4i32.micrometer(), 10.micrometer()];
    const BUS_WIDTH: [Length<f64>; 2] = [4.micrometer(), 10.micrometer()];
    const COLOR: [LayerData; 2] = [LayerData::new(1, 0), LayerData::new(1, 1)];
    const RESOLUTION: Resolution = Resolution::MinNumber(8001);
    const GAP: Length<f64> = MICROMETER;
    const RADIUS: Length<f64> = 240.micrometer();
    const PUL_ANG: Angle = Angle::FRAC_PI_6;
    let pul_rad: Length<f64> = RADIUS + RING_WIDTH[0] / 2. + BUS_WIDTH[0] / 2. + GAP;

    let ring = ArcCurve::new(
        CircularArc::new_origin(
            RADIUS,
            (Angle::from_deg(0.), Angle::from_deg(360.)),
            RESOLUTION,
        ),
        RING_WIDTH,
    );
    let mut cursor = CellCursor::new("topcell", Group::from(COLOR));
    cursor
        .mut_cell()
        .push(ring.into_group().color(Group::from(COLOR)));
    cursor.cursor.pos = [zero(), pul_rad].into();
    let bus_curve: ArcCurve<[_; 2]> = ArcCurve::new(
        CircularArc::new_origin(pul_rad, (Angle::from_deg(0.), -PUL_ANG / 2.), RESOLUTION),
        BUS_WIDTH.into(),
    );
    cursor.assemble_in(bus_curve.into_group());
    cursor.assemble_in(bus_curve.rev().into_group());
    cursor.cursor.pos = [zero(), pul_rad].into();
    cursor.cursor.dir = Angle::from_deg(180.);
    cursor.assemble_in(bus_curve.rev().into_group());
    cursor.assemble_in(bus_curve.into_group());
    cursor
        .into_cell()
        .save_as_lib(common::get_file_path("pulley.gds"))
        .unwrap();
}

#[test]
fn assemble_pulley() {
    common::init();
    const RING_WIDTH: [Length<f64>; 2] = [4.micrometer(), 10.micrometer()];
    const BUS_WIDTH: [Length<f64>; 2] = [4.micrometer(), 10.micrometer()];
    const COLOR: [LayerData; 2] = [LayerData::new(1, 0), LayerData::new(1, 1)];
    const RESOLUTION: Resolution = Resolution::MinNumber(8001);
    const GAP: Length<f64> = MICROMETER;
    const RADIUS: Length<f64> = 240.micrometer();
    const PUL_ANG: Angle = Angle::FRAC_PI_6;
    let pul_rad: Length<f64> = RADIUS + RING_WIDTH[0] / 2. + BUS_WIDTH[0] / 2. + GAP;

    let mut cursor = Assembler::new("topcell", COLOR, BUS_WIDTH, RESOLUTION);
    cursor.mut_cell().push(
        ArcCurve::new(
            CircularArc::new_origin(
                RADIUS,
                (Angle::from_deg(0.), Angle::from_deg(360.)),
                RESOLUTION,
            ),
            RING_WIDTH,
        )
        .into_array()
        .color(COLOR),
    );
    cursor
        .set_pos([zero(), pul_rad])
        .set_dir(Angle::from_deg(0.))
        .turn(pul_rad, -PUL_ANG / 2.)
        .turn(pul_rad, PUL_ANG / 2.)
        .extend(MICROMETER * 1000.)
        .taper(MICROMETER * 100., [MICROMETER * 2., MICROMETER * 8.])
        .extend(MICROMETER * 100.);
    cursor.width = BUS_WIDTH;
    cursor
        .set_pos([zero(), pul_rad])
        .set_dir(Angle::from_deg(180.))
        .turn(pul_rad, PUL_ANG / 2.)
        .turn(pul_rad, -PUL_ANG / 2.)
        .extend(MICROMETER * 1000.)
        .taper(MICROMETER * 100., [MICROMETER * 2., MICROMETER * 8.])
        .extend(MICROMETER * 100.);
    cursor
        .into_cell()
        .save_as_lib(common::get_file_path("assemble_pulley.gds"))
        .unwrap();
}

#[test]
fn taper_circ() {
    common::init();
    const COLOR: [LayerData; 2] = [LayerData::new(1, 0), LayerData::new(1, 1)];
    const RESOLUTION: Resolution = Resolution::MinNumber(8001);
    const BUS_WIDTH: [Length<f64>; 2] = [4.micrometer(), 10.micrometer()];

    let mut cursor = Assembler::new("topcell", COLOR, BUS_WIDTH, RESOLUTION);
    cursor
        .set_pos([zero(), zero()])
        .set_dir(Angle::from_deg(0.))
        .extend(100.micrometer())
        .turn(100.micrometer(), -Angle::PI)
        .tapered_with(
            CircularArc::new(
                100.micrometer(),
                [zero(), zero()],
                (Angle::zero(), Angle::PI),
                Resolution::MinDistance(20.nanometer()),
            ),
            [20.micrometer(), 13.micrometer()],
        )
        .extend(100.micrometer())
        .taper_arc(
            100.micrometer(),
            -Angle::FRAC_PI_2,
            [19.micrometer(), 40.micrometer()],
        );

    cursor
        .into_cell()
        .save_as_lib(common::get_file_path("taper_circ.gds"))
        .unwrap();
}
