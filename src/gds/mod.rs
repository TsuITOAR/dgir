use std::{cell::RefCell, collections::BTreeMap, fmt::Debug, rc::Rc};

use gds21::GdsStrans;
use num::{traits::FloatConst, Float, FromPrimitive, ToPrimitive};

use crate::{
    color::LayerData,
    draw::coordinate::Coordinate,
    units::{Angle, Length},
    zero, Num, Quantity,
};

use togds::ToGds21Library;

mod togds;

//const DISPLAY_POINTS_NUM: usize = 20;
type Result<T> = gds21::GdsResult<T>;
type Points<Q> = Box<dyn Iterator<Item = Coordinate<Q>>>;

pub struct Path<Q: Quantity> {
    pub curve: Points<Q>,
    pub color: LayerData,
    pub width: Option<Q>,
}
/*
type Points<Q> = Box<dyn ToPoints<Q>>;

use dyn_clone::DynClone;
pub trait ToPoints<Q: Quantity>: DynClone + Iterator<Item = Coordinate<Q>> {
    fn dyn_to_points(self: Box<Self>, scale: Q) -> Vec<gds21::GdsPoint>;
}

impl<Q: Quantity, T, N: Num> ToPoints<Q> for T
where
    T: Iterator<Item = Coordinate<Q>> + Clone,
    Q: Div<Output = N> + Clone,
{
    fn dyn_to_points(self: Box<Self>, scale: Q) -> Vec<gds21::GdsPoint> {
        self.map(|pos| gds21::GdsPoint {
            x: (pos[0] / scale.clone()).to_i32().unwrap(),
            y: (pos[1] / scale.clone()).to_i32().unwrap(),
        })
        .collect()
    }
}

dyn_clone::clone_trait_object!(<Q:Quantity> ToPoints<Q>);
 */

impl<Q> Debug for Path<Q>
where
    Q: Quantity,
{
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        write!(
            f,
            "Path {{ curve: ..., (layer, datatype): {}, width: {:?} }}",
            self.color, self.width
        )
    }
}

pub struct Polygon<Q: Quantity> {
    pub area: Points<Q>,
    pub color: LayerData,
}

impl<Q> Debug for Polygon<Q>
where
    Q: Quantity,
{
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        write!(f, "Path {{ curve: ..., (layer, datatype): {}}}", self.color)
    }
}

#[derive(Clone, Debug)]
pub struct Ref<Q>
where
    Q: Quantity,
{
    pub strans: Option<gds21::GdsStrans>,
    pub(crate) pos: Coordinate<Q>,
    pub(crate) id: String,
    pub(crate) dep: BTreeMap<String, Rc<RefCell<DgirCell<Q>>>>, //TODO need to avoid circular ref, or dead loop happens
}

impl<Q: Quantity> Ref<Q> {
    pub fn set_pos<C: Into<Coordinate<Q>>>(&mut self, c: C) -> &mut Self {
        self.pos = c.into();
        self
    }

    /// Reflection, about the x-axis.
    /// Applied before rotation.
    pub fn set_reflect(&mut self, reflected: bool) -> &mut Self {
        if let Some(ref mut s) = self.strans {
            s.reflected = reflected;
        } else {
            self.strans = GdsStrans {
                reflected,
                ..Default::default()
            }
            .into()
        }
        self
    }
    pub fn set_rot<T: Num + FloatConst + FromPrimitive>(&mut self, ang: Angle<T>) -> &mut Self {
        if let Some(ref mut s) = self.strans {
            s.angle = ang.to_deg().to_f64().unwrap().into();
        } else {
            self.strans = GdsStrans {
                angle: ang.to_deg().to_f64().unwrap().into(),
                ..Default::default()
            }
            .into()
        }
        self
    }

    pub fn into_array_ref(
        &self,
        start: impl Into<Coordinate<Q>>,
        rows: i16,
        row_end: impl Into<Coordinate<Q>>,
        cols: i16,
        col_end: impl Into<Coordinate<Q>>,
    ) -> ArrayRef<Q> {
        ArrayRef {
            strans: self.strans.clone(),
            rows,
            cols,
            start: start.into(),
            col_end: col_end.into(),
            row_end: row_end.into(),
            id: self.id.clone(),
            dep: self.dep.clone(),
        }
    }
}

#[derive(Debug, Clone)]
pub struct ArrayRef<Q>
where
    Q: Quantity,
{
    pub(crate) strans: Option<gds21::GdsStrans>,
    pub(crate) rows: i16,
    pub(crate) cols: i16,
    pub(crate) start: Coordinate<Q>,
    pub(crate) col_end: Coordinate<Q>,
    pub(crate) row_end: Coordinate<Q>,
    pub(crate) id: String,
    pub(crate) dep: BTreeMap<String, Rc<RefCell<DgirCell<Q>>>>, //TODO need to avoid circular ref, or dead loop happens
}

impl<Q: Quantity> ArrayRef<Q> {
    pub fn set_start<C: Into<Coordinate<Q>>>(&mut self, c: C) -> &mut Self {
        self.start = c.into();
        self
    }
    pub fn set_col_end<C: Into<Coordinate<Q>>>(&mut self, c: C) -> &mut Self {
        self.col_end = c.into();
        self
    }
    pub fn set_row_end<C: Into<Coordinate<Q>>>(&mut self, c: C) -> &mut Self {
        self.row_end = c.into();
        self
    }
    pub fn set_rot<T: Num + FloatConst + FromPrimitive>(&mut self, ang: Angle<T>) -> &mut Self {
        if let Some(ref mut s) = self.strans {
            s.angle = ang.to_deg().to_f64().unwrap().into();
        } else {
            self.strans = GdsStrans {
                angle: ang.to_deg().to_f64().unwrap().into(),
                ..Default::default()
            }
            .into()
        }
        self
    }
}

#[derive(Clone, Debug)]
pub struct Text<Q: Quantity> {
    pub(crate) content: String,
    pub(crate) strans: Option<gds21::GdsStrans>,
    pub(crate) pos: Coordinate<Q>,
    pub(crate) width: Option<Q>,
    pub layer: i16,
    pub path_type: Option<i16>,
    pub texttype: i16,
}

impl<Q: Quantity> Text<Q> {
    pub fn new(
        content: String,
        pos: impl Into<Coordinate<Q>>,
        layer: i16,
        width: impl Into<Option<Q>>,
    ) -> Self {
        Self {
            content,
            pos: pos.into(),
            width: width.into(),
            layer,
            strans: None,
            path_type: None,
            texttype: 1,
        }
    }
}

pub trait InnerPoints<T: Num + Float>: Sized {
    fn points_mut(&mut self) -> &mut Points<Length<T>>;
    fn translate(&mut self, t: Coordinate<Length<T>>) -> &mut Self {
        let s = self.points_mut();
        let mut ts =
            Box::new([Coordinate::from([zero(), zero()])].into_iter()) as Points<Length<T>>;
        std::mem::swap(s, &mut ts);
        let ts = Box::new(ts.map(move |x| x.translate(t))) as Points<Length<T>>;
        *self.points_mut() = ts;
        self
    }
    fn rotate(&mut self, a: Angle<T>) -> &mut Self {
        let s = self.points_mut();
        let mut ts =
            Box::new([Coordinate::from([zero(), zero()])].into_iter()) as Points<Length<T>>;
        std::mem::swap(s, &mut ts);
        let ts = Box::new(ts.map(move |x| x.rotate(a))) as Points<Length<T>>;
        *self.points_mut() = ts;
        self
    }
    fn reflect(&mut self) -> &mut Self {
        let s = self.points_mut();
        let mut ts =
            Box::new([Coordinate::from([zero(), zero()])].into_iter()) as Points<Length<T>>;
        std::mem::swap(s, &mut ts);
        let ts = Box::new(ts.map(move |x| [x[0], -x[1]].into())) as _;
        *self.points_mut() = ts;
        self
    }
}

macro_rules! implInnerPoints {
    ($( $id:ident.$field:ident),*) => {
        $(
            impl<T: Num + Float> InnerPoints<T> for $id<Length<T>> {
                fn points_mut(&mut self) -> &mut Points<Length<T>> {
                    &mut self.$field
                }
            }
        )*
    };
}

pub trait InnerStrans<T: Num + Float>: Sized {
    fn strans(&mut self) -> &mut Option<gds21::GdsStrans>;
    fn pos(&mut self) -> &mut Coordinate<Length<T>>;
    fn translate<C: Into<Coordinate<Length<T>>>>(&mut self, c: C) -> &mut Self {
        *self.pos() = self.pos().translate(c.into());
        self
    }
    fn rotate(&mut self, ang: Angle<T>) -> &mut Self
    where
        T: FloatConst + FromPrimitive,
    {
        if let Some(ref mut s) = self.strans() {
            *s.angle.get_or_insert(0.) += ang.to_deg().to_f64().unwrap();
        } else {
            *self.strans() = GdsStrans {
                angle: ang.to_deg().to_f64().unwrap().into(),
                ..Default::default()
            }
            .into()
        }
        // to unify the behavior of ref objects and real objects
        *self.pos() = self.pos().rotate(ang);
        self
    }
    fn reflect(&mut self) -> &mut Self {
        if let Some(ref mut s) = self.strans() {
            s.reflected = !s.reflected;
        } else {
            *self.strans() = GdsStrans {
                reflected: true,
                ..Default::default()
            }
            .into()
        }
        // to unify the behavior of ref objects and real objects
        *self.pos() = [self.pos()[0], -self.pos()[1]].into();
        self
    }
}

macro_rules! implInnerStrans {
    ($( $id:ident.$field:ident),*) => {
        $(
            impl<T: Num + Float> InnerStrans<T> for $id<Length<T>> {
                fn strans(&mut self) -> &mut Option<gds21::GdsStrans>{
                    &mut self.strans
                }
                fn pos(&mut self) -> &mut Coordinate<Length<T>>{
                    &mut self.$field
                }
            }
        )*
    };
}

implInnerPoints!(Path.curve, Polygon.area);

implInnerStrans!(Ref.pos, ArrayRef.start, Text.pos);

#[derive(Debug)]
pub enum Element<Q>
where
    Q: Quantity,
{
    Path(Path<Q>),
    Polygon(Polygon<Q>),
    Ref(Ref<Q>),
    ARef(ArrayRef<Q>),
    Text(Text<Q>),
}

impl<T: Num + Float> Element<Length<T>> {
    pub fn reflect(&mut self) -> &mut Self {
        match self {
            Element::Path(p) => {
                p.reflect();
            }
            Element::Polygon(p) => {
                p.reflect();
            }
            Element::Ref(p) => {
                p.reflect();
            }
            Element::ARef(p) => {
                p.reflect();
            }
            Element::Text(p) => {
                p.reflect();
            }
        };
        self
    }
    pub fn translate(&mut self, t: Coordinate<Length<T>>) -> &mut Self {
        match self {
            Element::Path(p) => {
                p.translate(t);
            }
            Element::Polygon(p) => {
                p.translate(t);
            }
            Element::Ref(p) => {
                p.translate(t);
            }
            Element::ARef(p) => {
                p.translate(t);
            }
            Element::Text(p) => {
                p.translate(t);
            }
        };
        self
    }
    pub fn rotate(&mut self, a: Angle<T>) -> &mut Self
    where
        T: FloatConst + FromPrimitive,
    {
        match self {
            Element::Path(p) => {
                p.rotate(a);
            }
            Element::Polygon(p) => {
                p.rotate(a);
            }
            Element::Ref(p) => {
                p.rotate(a);
            }
            Element::ARef(p) => {
                p.rotate(a);
            }
            Element::Text(p) => {
                p.rotate(a);
            }
        };
        self
    }
}

impl<Q> Element<Q>
where
    Q: Quantity,
{
    pub fn into_cell<S: ToString>(self, name: S) -> DgirCell<Q> {
        DgirCell {
            name: name.to_string(),
            elements: vec![self],
        }
    }
}

#[derive(Debug)]
pub enum ElementsGroup<Q>
where
    Q: Quantity,
{
    Single(Element<Q>),
    Group(Vec<Element<Q>>),
}

impl<T: Num + Float> ElementsGroup<Length<T>> {
    pub fn reflect(&mut self) -> &mut Self {
        match self {
            ElementsGroup::Single(s) => {
                s.reflect();
            }
            ElementsGroup::Group(g) => {
                g.iter_mut().for_each(|g| {
                    g.reflect();
                });
            }
        };
        self
    }
    pub fn translate(&mut self, t: Coordinate<Length<T>>) -> &mut Self {
        match self {
            ElementsGroup::Single(s) => {
                s.translate(t);
            }
            ElementsGroup::Group(g) => {
                g.iter_mut().for_each(|g| {
                    g.translate(t);
                });
            }
        };
        self
    }
    pub fn rotate(&mut self, a: Angle<T>) -> &mut Self
    where
        T: FloatConst + FromPrimitive,
    {
        match self {
            ElementsGroup::Single(s) => {
                s.rotate(a);
            }
            ElementsGroup::Group(g) => {
                g.iter_mut().for_each(|g| {
                    g.rotate(a);
                });
            }
        };
        self
    }
}

impl<Q: Quantity> IntoIterator for ElementsGroup<Q> {
    type IntoIter = <Vec<Element<Q>> as IntoIterator>::IntoIter;
    type Item = Element<Q>;
    fn into_iter(self) -> Self::IntoIter {
        match self {
            ElementsGroup::Single(s) => vec![s].into_iter(),
            ElementsGroup::Group(g) => g.into_iter(),
        }
    }
}

impl<Q: Quantity> Default for ElementsGroup<Q> {
    fn default() -> Self {
        Self::Group(Vec::new())
    }
}

impl<Q: Quantity> From<Vec<ElementsGroup<Q>>> for ElementsGroup<Q> {
    fn from(s: Vec<ElementsGroup<Q>>) -> Self {
        let mut r = Vec::new();
        for i in s {
            match i {
                ElementsGroup::Single(a) => r.push(a),
                ElementsGroup::Group(mut g) => r.append(&mut g),
            }
        }
        Self::from(r)
    }
}

impl<Q: Quantity> ElementsGroup<Q> {
    pub(crate) fn as_vec(&mut self) -> &mut Vec<Element<Q>> {
        let s = std::mem::take(self);
        let g = match s {
            Self::Group(g) => g,
            Self::Single(s) => vec![s],
        };
        *self = g.into();
        if let Self::Group(g) = self {
            g
        } else {
            unreachable!()
        }
    }
    pub(crate) fn into_vec(self) -> Vec<Element<Q>> {
        match self {
            Self::Single(s) => vec![s],
            Self::Group(g) => g,
        }
    }
    pub(crate) fn extend(&mut self, other: Self) -> &mut Self {
        self.as_vec().extend(other.into_vec());
        self
    }
}

impl<Q> From<Element<Q>> for ElementsGroup<Q>
where
    Q: Quantity,
{
    fn from(e: Element<Q>) -> Self {
        ElementsGroup::Single(e)
    }
}

impl<Q> From<Vec<Element<Q>>> for ElementsGroup<Q>
where
    Q: Quantity,
{
    fn from(e: Vec<Element<Q>>) -> Self {
        ElementsGroup::Group(e)
    }
}

impl<Q> From<Path<Q>> for Element<Q>
where
    Q: Quantity,
{
    fn from(p: Path<Q>) -> Self {
        Element::Path(p)
    }
}

impl<Q> From<Polygon<Q>> for Element<Q>
where
    Q: Quantity,
{
    fn from(p: Polygon<Q>) -> Self {
        Element::Polygon(p)
    }
}

impl<Q> From<Ref<Q>> for Element<Q>
where
    Q: Quantity,
{
    fn from(r: Ref<Q>) -> Self {
        Element::Ref(r)
    }
}
impl<Q> From<ArrayRef<Q>> for Element<Q>
where
    Q: Quantity,
{
    fn from(r: ArrayRef<Q>) -> Self {
        Element::ARef(r)
    }
}

impl<Q> From<Text<Q>> for Element<Q>
where
    Q: Quantity,
{
    fn from(r: Text<Q>) -> Self {
        Element::Text(r)
    }
}

impl<Q> From<Path<Q>> for ElementsGroup<Q>
where
    Q: Quantity,
{
    fn from(p: Path<Q>) -> Self {
        Element::Path(p).into()
    }
}

impl<Q> From<Polygon<Q>> for ElementsGroup<Q>
where
    Q: Quantity,
{
    fn from(p: Polygon<Q>) -> Self {
        Element::Polygon(p).into()
    }
}

impl<Q> From<Ref<Q>> for ElementsGroup<Q>
where
    Q: Quantity,
{
    fn from(r: Ref<Q>) -> Self {
        Element::Ref(r).into()
    }
}

impl<Q> From<ArrayRef<Q>> for ElementsGroup<Q>
where
    Q: Quantity,
{
    fn from(r: ArrayRef<Q>) -> Self {
        Element::ARef(r).into()
    }
}

impl<Q> From<Text<Q>> for ElementsGroup<Q>
where
    Q: Quantity,
{
    fn from(r: Text<Q>) -> Self {
        Element::Text(r).into()
    }
}

#[derive(Debug)]
pub struct DgirCell<Q = Length<f64>>
where
    Q: Quantity,
{
    pub(crate) name: String,
    pub(crate) elements: Vec<Element<Q>>,
}

impl<Q: Quantity> AsMut<DgirCell<Q>> for DgirCell<Q> {
    fn as_mut(&mut self) -> &mut DgirCell<Q> {
        self
    }
}

impl<Q> DgirCell<Q>
where
    Q: Quantity,
{
    #[cfg(not(feature = "hash-name"))]
    pub fn new(name: impl ToString) -> Self {
        Self {
            name: name.to_string(),
            elements: Vec::new(),
        }
    }

    #[cfg(feature = "hash-name")]
    pub fn new(name: impl ToString) -> Self {
        use std::hash::Hasher;
        Self {
            name: {
                let mut hasher = std::collections::hash_map::DefaultHasher::new();
                hasher.write(name.to_string().as_bytes());
                format!("{:X}", hasher.finish())
            },
            elements: Vec::new(),
        }
    }

    pub fn new_raw(name: impl ToString) -> Self {
        Self {
            name: name.to_string(),
            elements: Vec::new(),
        }
    }

    pub fn get_name(&self) -> &str {
        &self.name
    }

    pub fn modify_name(&mut self, modify: impl FnOnce(String) -> String) -> &mut Self {
        #[cfg(feature = "hash-name")]
        {
            use std::hash::Hasher;
            self.name = {
                let mut hasher = std::collections::hash_map::DefaultHasher::new();
                hasher.write(modify(self.name.clone()).as_bytes());
                format!("{:X}", hasher.finish())
            };
        }
        #[cfg(not(feature = "hash-name"))]
        {
            self.name = modify(self.name.clone());
        }
        self
    }

    pub fn rename(&mut self, name: String) -> &mut Self {
        #[cfg(feature = "hash-name")]
        {
            use std::hash::Hasher;
            self.name = {
                let mut hasher = std::collections::hash_map::DefaultHasher::new();
                hasher.write(name.as_bytes());
                format!("{:X}", hasher.finish())
            };
        }
        #[cfg(not(feature = "hash-name"))]
        {
            self.name = name;
        }
        self
    }
    pub fn push<U: Into<ElementsGroup<Q>>>(&mut self, element: U) -> &mut Self {
        match element.into() {
            ElementsGroup::Single(s) => self.elements.push(s),
            ElementsGroup::Group(g) => self.elements.extend(g),
        }
        self
    }

    pub fn into_ref(self) -> Ref<Q> {
        let mut s = self;
        let name = s.name.clone();
        let mut dep = s.get_dependencies();
        dep.insert(name.clone(), Rc::new(s.into()));
        Ref {
            strans: None,
            dep,
            pos: Coordinate::from([Q::zero(), Q::zero()]),
            id: name,
        }
    }
    pub fn into_ref_at(self, pos: impl Into<Coordinate<Q>>) -> Ref<Q> {
        let mut s = self;
        let name = s.name.clone();
        let mut dep = s.get_dependencies();
        dep.insert(name.clone(), Rc::new(s.into()));
        Ref {
            strans: None,
            dep,
            pos: pos.into(),
            id: name,
        }
    }
    pub fn into_array_ref(
        self,
        start: impl Into<Coordinate<Q>>,
        rows: i16,
        row_end: impl Into<Coordinate<Q>>,
        cols: i16,
        col_end: impl Into<Coordinate<Q>>,
    ) -> ArrayRef<Q> {
        let mut s = self;
        let mut dep = s.get_dependencies();
        let name = s.name.clone();
        dep.insert(name.clone(), Rc::new(s.into()));
        ArrayRef {
            rows,
            cols,
            start: start.into(),
            col_end: col_end.into(),
            row_end: row_end.into(),
            id: name,
            dep,
            strans: None,
        }
    }
    //make sure every sub dependencies is empty
    pub(crate) fn get_dependencies(&mut self) -> BTreeMap<String, Rc<RefCell<DgirCell<Q>>>> {
        let mut dependencies = BTreeMap::new();
        for element in self.elements.iter_mut() {
            match element {
                Element::Ref(Ref { dep: ref mut d, .. }) => {
                    debug_assert!(is_sub_dependencies_empty(d));
                    dependencies.append(d);
                }
                Element::ARef(ArrayRef { dep: ref mut d, .. }) => {
                    debug_assert!(is_sub_dependencies_empty(d));
                    dependencies.append(d);
                }
                _ => (),
            }
        }
        dependencies
    }
}

impl<T: Num + FromPrimitive + ToPrimitive> DgirCell<Length<T>> {
    pub fn save_as_lib(self, filename: impl AsRef<std::path::Path>) -> Result<()> {
        DgirLibrary {
            name: None,
            units: DgirUnits::default(),
            cells: vec![self],
        }
        .save(filename)
    }
}

fn is_sub_dependencies_empty<Q: Quantity>(
    set: &BTreeMap<String, Rc<RefCell<DgirCell<Q>>>>,
) -> bool {
    set.iter().all(|c| {
        c.1.borrow().elements.iter().all(|e| match e {
            Element::Ref(Ref {
                dep: dependencies, ..
            }) => dependencies.is_empty(),
            _ => true,
        })
    })
}

impl<Q> PartialEq for DgirCell<Q>
where
    Q: Quantity,
{
    fn eq(&self, other: &Self) -> bool {
        self.name.eq(&other.name)
    }
}
impl<Q> Eq for DgirCell<Q> where Q: Quantity {}

impl<Q> PartialOrd for DgirCell<Q>
where
    Q: Quantity,
{
    fn partial_cmp(&self, other: &Self) -> Option<std::cmp::Ordering> {
        Some(self.cmp(other))
    }
}
impl<Q> Ord for DgirCell<Q>
where
    Q: Quantity,
{
    fn cmp(&self, other: &Self) -> std::cmp::Ordering {
        self.name.cmp(&other.name)
    }
}

impl<Q> AsRef<Vec<Element<Q>>> for DgirCell<Q>
where
    Q: Quantity,
{
    fn as_ref(&self) -> &Vec<Element<Q>> {
        &self.elements
    }
}

impl<Q> AsMut<Vec<Element<Q>>> for DgirCell<Q>
where
    Q: Quantity,
{
    fn as_mut(&mut self) -> &mut Vec<Element<Q>> {
        &mut self.elements
    }
}

#[derive(Debug, Clone, Copy, PartialEq, PartialOrd, Eq)]
pub struct DgirUnits<Q>
where
    Q: Quantity,
{
    database: Q,
    user: Q,
}

impl<T> Default for DgirUnits<Length<T>>
where
    T: Num + FromPrimitive,
{
    fn default() -> Self {
        Self {
            database: Length::new::<crate::units::Nanometer>(T::one()),
            user: Length::new::<crate::units::Micrometer>(T::one()),
        }
    }
}

#[derive(Debug)]
pub struct DgirLibrary<Q>
where
    Q: Quantity,
{
    pub name: Option<String>,
    pub(crate) units: DgirUnits<Q>,
    pub(crate) cells: Vec<DgirCell<Q>>,
}

impl<T> Default for DgirLibrary<Length<T>>
where
    T: Num + FromPrimitive,
{
    fn default() -> Self {
        Self {
            name: None,
            units: DgirUnits::default(),
            cells: Vec::new(),
        }
    }
}

impl<T> DgirLibrary<Length<T>>
where
    T: Num + FromPrimitive,
{
    pub fn new<S: ToString>(name: S) -> Self {
        Self {
            name: Some(name.to_string()),
            ..Default::default()
        }
    }
    pub fn set_database_unit(&mut self, db_len: Length<T>) -> &mut Self {
        self.units.database = db_len;
        self
    }
    pub fn set_user_unit(&mut self, user_len: Length<T>) -> &mut Self {
        self.units.user = user_len;
        self
    }
    pub fn push<C: Into<DgirCell<Length<T>>>>(&mut self, cell: C) -> &mut Self {
        self.cells.push(cell.into());
        self
    }
}

impl<T> DgirLibrary<Length<T>>
where
    T: Num + FromPrimitive + ToPrimitive,
{
    pub fn save(self, filename: impl AsRef<std::path::Path>) -> Result<()> {
        self.to_gds21_library().save(filename)
    }
    pub fn to_library(self) -> gds21::GdsLibrary {
        self.to_gds21_library()
    }
}
