use gds21::GdsPoint as Gds21Point;
use num::{FromPrimitive, ToPrimitive};
use std::{cell::RefCell, marker::PhantomData, ops::Index, rc::Rc};

use crate::{
    close_curve,
    draw::coordinate::{Coordinate, Point},
    gds::Element,
    points_num_check, split_path, split_polygon,
    units::Length,
    Num, MAX_POINTS_NUM,
};

use super::DgirCell;

pub(crate) trait ToGds21Points: Iterator {
    type Scale: Clone;
    fn to_gds21_points(self, scale: Self::Scale) -> Vec<Gds21Point>;
}

//helper trait to constrain type of iterators which give different type of length
//see https://stackoverflow.com/questions/34470995/how-to-allow-multiple-implementations-of-a-trait-on-various-types-of-intoiterato
//see https://github.com/rust-lang/rust/issues/31844
//see https://stackoverflow.com/questions/40392524/conflicting-trait-implementations-even-though-associated-types-differ
pub(crate) trait CoordinateIterator {
    type Length;
    type Scale: Copy;
    type Scalar: ToPrimitive;
    type Coordinate: Index<usize, Output = Self::Length>;
    fn after_scale(coor: Self::Coordinate, scale: Self::Scale) -> Gds21Point;
}

impl<C, T: Num> CoordinateIterator for (C, Point<T>) {
    type Length = Length<T>;
    type Scale = Length<T>;
    type Scalar = T;
    type Coordinate = Point<T>;
    fn after_scale(coor: Self::Coordinate, scale: Self::Scale) -> Gds21Point {
        Gds21Point {
            x: (coor[0] / scale)
                .to_f64()
                .unwrap_or_else(warn_nan_coordinate)
                .round() as _,
            y: (coor[1] / scale)
                .to_f64()
                .unwrap_or_else(warn_nan_coordinate)
                .round() as _,
        }
    }
}

fn warn_nan_coordinate() -> f64 {
    use log::error;
    error!("a NaN value occurred, try setting as 0. to continue");
    0.
}
impl<I> ToGds21Points for I
where
    I: Iterator,
    (I, I::Item): CoordinateIterator<Coordinate = I::Item>,
{
    type Scale = <(I, I::Item) as CoordinateIterator>::Scale;
    fn to_gds21_points(self, scale: Self::Scale) -> Vec<Gds21Point> {
        self.map(|x| <(I, I::Item)>::after_scale(x, scale))
            .collect()
    }
}

pub(crate) trait ToGds21Struct {
    type Scale;
    fn to_gds21_struct(self, scale: Self::Scale) -> gds21::GdsStruct;
}

impl<T> ToGds21Struct for DgirCell<Length<T>>
where
    T: Num,
{
    type Scale = Length<T>;
    fn to_gds21_struct(self, scale: Self::Scale) -> gds21::GdsStruct {
        use gds21::*;
        fn to_gds_point<T: ToPrimitive + Num>(
            f: Coordinate<Length<T>>,
            scale: Length<T>,
        ) -> Gds21Point {
            Gds21Point::new(
                (f[0] / scale)
                    .to_f64()
                    .unwrap_or_else(warn_nan_coordinate)
                    .round() as _,
                (f[1] / scale)
                    .to_f64()
                    .unwrap_or_else(warn_nan_coordinate)
                    .round() as _,
            )
        }

        let mut new_cell = GdsStruct::new(self.name);
        for elem in self.elements {
            match elem {
                Element::Path(p) => {
                    let xy = p.curve.to_gds21_points(scale);
                    if points_num_check(&xy) {
                        new_cell.elems.push(GdsElement::GdsPath({
                            GdsPath {
                                layer: p.color.layer,
                                datatype: p.color.datatype,
                                xy,
                                width: p.width.map(|l| {
                                    (l / scale)
                                        .to_f64()
                                        .unwrap_or_else(warn_nan_coordinate)
                                        .round() as _
                                }),
                                ..Default::default()
                            }
                        }))
                    } else {
                        let width = p.width.map(|l| {
                            (l / scale)
                                .to_f64()
                                .unwrap_or_else(warn_nan_coordinate)
                                .round() as _
                        });
                        for c in split_path(xy, MAX_POINTS_NUM) {
                            new_cell.elems.push(GdsElement::GdsPath({
                                GdsPath {
                                    layer: p.color.layer,
                                    datatype: p.color.datatype,
                                    xy: c,
                                    width,
                                    ..Default::default()
                                }
                            }))
                        }
                    }
                }
                Element::Polygon(p) => {
                    let mut xy = p.area.to_gds21_points(scale);
                    close_curve(&mut xy); //TODO: add a notify layer data if curve not closed

                    if points_num_check(&xy) {
                        new_cell.elems.push(GdsElement::GdsBoundary({
                            GdsBoundary {
                                layer: p.color.layer,
                                datatype: p.color.datatype,
                                xy,
                                ..Default::default()
                            }
                        }))
                    } else {
                        for c in split_polygon(xy, MAX_POINTS_NUM) {
                            new_cell.elems.push(GdsElement::GdsBoundary({
                                GdsBoundary {
                                    layer: p.color.layer,
                                    datatype: p.color.datatype,
                                    xy: c,
                                    ..Default::default()
                                }
                            }))
                        }
                    }
                }
                Element::Ref(r) => new_cell.elems.push(GdsElement::GdsStructRef(GdsStructRef {
                    name: r.id,
                    xy: to_gds_point(r.pos, scale),
                    strans: r.strans,
                    ..Default::default()
                })),
                Element::ARef(ar) => new_cell.elems.push(GdsElement::GdsArrayRef(GdsArrayRef {
                    name: ar.id,
                    xy: [
                        to_gds_point(ar.start, scale),
                        to_gds_point(ar.col_end, scale),
                        to_gds_point(ar.row_end, scale),
                    ],
                    cols: ar.rows,
                    rows: ar.cols,
                    strans: ar.strans,
                    ..Default::default()
                })),
                Element::Text(t) => new_cell.elems.push(GdsElement::GdsTextElem(GdsTextElem {
                    string: t.content,
                    layer: t.layer,
                    xy: to_gds_point(t.pos, scale),
                    width: t.width.map(|x| {
                        (x / scale)
                            .to_f64()
                            .unwrap_or_else(warn_nan_coordinate)
                            .round() as _
                    }),
                    strans: t.strans,
                    path_type: t.path_type,
                    texttype: t.texttype,
                    ..Default::default()
                })),
            }
        }
        new_cell
    }
}

pub(crate) trait ToGds21Library {
    fn to_gds21_library(self) -> gds21::GdsLibrary;
}

struct PlaceHolderPointsIter<T> {
    p: PhantomData<T>,
}

impl<T> Default for PlaceHolderPointsIter<T> {
    fn default() -> Self {
        Self { p: PhantomData }
    }
}

impl<T> Iterator for PlaceHolderPointsIter<T> {
    type Item = T;
    fn next(&mut self) -> Option<Self::Item> {
        unreachable!("using placeholder")
    }
}

fn get_cell<T: Num>(src: (String, Rc<RefCell<DgirCell<Length<T>>>>)) -> DgirCell<Length<T>> {
    let mut src1 = src.1.borrow_mut();

    DgirCell {
        name: src.0,
        elements: std::mem::take(&mut src1.elements),
    }
}

impl<T> ToGds21Library for super::DgirLibrary<Length<T>>
where
    T: Num + FromPrimitive,
{
    fn to_gds21_library(self) -> gds21::GdsLibrary {
        gds21::GdsLibrary {
            name: self.name.unwrap_or_else(|| {
                self.cells
                    .first()
                    .map(|x| x.name.clone())
                    .unwrap_or_default()
            }),
            structs: {
                let database_unit = self.units.database;
                let mut cells = self.cells.into_iter();
                match cells.next() {
                    None => Vec::new(),
                    Some(mut first_cell) => {
                        let mut dependencies = first_cell.get_dependencies();
                        for mut cell in cells {
                            //if only one topcell is expected, all of its dependencies should be inside itself
                            dependencies.append(&mut cell.get_dependencies());
                            dependencies.insert(cell.get_name().to_string(), Rc::new(cell.into()));
                        }
                        let mut structs = Vec::with_capacity(dependencies.len() + 1);
                        debug_assert!(!dependencies.contains_key(&first_cell.name));
                        structs.push(first_cell.to_gds21_struct(database_unit));
                        for s in dependencies {
                            structs.push(get_cell(s).to_gds21_struct(database_unit));
                        }
                        structs
                    }
                }
            },
            units: gds21::GdsUnits::new(
                (self.units.database / self.units.user).to_f64().unwrap(),
                (self.units.database / Length::new::<crate::units::Meter>(T::one()))
                    .to_f64()
                    .unwrap(),
            ),
            ..Default::default()
        }
    }
}
