use core::f64;
use std::{
    fmt::{Debug, Display},
    ops::{Add, AddAssign, Div, DivAssign, Mul, MulAssign, Neg, Sub, SubAssign},
};

use float_cmp::ApproxEq;
use num::{traits::FloatConst, Float, FromPrimitive, Num, Signed, Zero};

#[derive(Clone, Copy, Default)]
#[repr(transparent)]
pub struct Length<S> {
    pub(crate) value: S,
}

impl<S: Signed> Length<S> {
    // ? should use accept type `self`
    // ? but that could cause compile error `destructors cannot be evaluated at compile-time`
    pub fn abs(&self) -> Self
    where
        S: Signed,
    {
        Self {
            value: self.value.abs(),
        }
    }
    pub fn is_negative(&self) -> bool
    where
        S: Signed,
    {
        self.value.is_negative()
    }
    pub fn is_positive(&self) -> bool
    where
        S: Signed,
    {
        self.value.is_positive()
    }
    pub fn abs_sub(&self, other: &Self) -> Self
    where
        S: Signed,
    {
        Self {
            value: self.value.abs_sub(&other.value),
        }
    }
    pub fn signum(&self) -> S
    where
        S: Signed,
    {
        self.value.signum()
    }
}

impl<S: Float> Length<S> {
    /// Computes the four quadrant arctangent of `self` (`y`) and `other` (`x`).
    ///
    /// * `x = 0`, `y = 0`: `0`
    /// * `x >= 0`: `arctan(y/x)` -> `[-pi/2, pi/2]`
    /// * `y >= 0`: `arctan(y/x) + pi` -> `(pi/2, pi]`
    /// * `y < 0`: `arctan(y/x) - pi` -> `(-pi, -pi/2)`
    pub fn atan2(self, other: Self) -> Angle<S>
    where
        S: Float,
    {
        Angle::from_rad(self.value.atan2(other.value))
    }
}

impl<S: PartialOrd> Length<S> {
    pub fn max<'a>(&'a self, other: &'a Self) -> &'a Self
    where
        S: PartialOrd,
    {
        if self.value > other.value {
            self
        } else {
            other
        }
    }
    pub fn min<'a>(&'a self, other: &'a Self) -> &'a Self
    where
        S: PartialOrd,
    {
        if self.value < other.value {
            self
        } else {
            other
        }
    }
}

impl<S: Display> Display for Length<S> {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        write!(f, "{}", self.value)
    }
}

impl<S: Display> Debug for Length<S> {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        write!(f, "{}", self.value)
    }
}

impl<M, S> float_cmp::ApproxEq for Length<S>
where
    M: Copy + Default + float_cmp::FloatMargin,
    S: float_cmp::ApproxEq<Margin = M>,
{
    type Margin = M;
    fn approx_eq<N: Into<Self::Margin>>(self, other: Self, margin: N) -> bool {
        let margin = margin.into();
        self.value.approx_eq(other.value, margin)
    }
}

impl<S: Num> Length<S> {
    pub fn new<U: Unit>(value: S) -> Length<S>
    where
        S: Mul<S, Output = S> + FromPrimitive,
    {
        Length {
            value: value * S::from_f64(U::CONVERSION_FACTOR).unwrap(),
        }
    }
}

impl<S: Neg<Output = S>> Neg for Length<S> {
    type Output = Length<S>;
    fn neg(self) -> Self::Output {
        Length::<S> {
            value: self.value.neg(),
        }
    }
}

impl<S: Add<Output = S>> Add<Length<S>> for Length<S> {
    type Output = Length<S>;
    fn add(self, rhs: Length<S>) -> Self::Output {
        Self {
            value: self.value + rhs.value,
        }
    }
}

impl<S: AddAssign> AddAssign<Length<S>> for Length<S> {
    fn add_assign(&mut self, rhs: Length<S>) {
        self.value += rhs.value;
    }
}

impl<S: Sub<Output = S>> Sub<Length<S>> for Length<S> {
    type Output = Length<S>;
    fn sub(self, rhs: Length<S>) -> Self::Output {
        Self {
            value: self.value - rhs.value,
        }
    }
}

impl<S: SubAssign> SubAssign<Length<S>> for Length<S> {
    fn sub_assign(&mut self, rhs: Length<S>) {
        self.value -= rhs.value;
    }
}

impl<S: Mul<Output = S>> Mul<S> for Length<S> {
    type Output = Length<S>;
    fn mul(self, rhs: S) -> Self::Output {
        Self {
            value: self.value * rhs,
        }
    }
}

impl Mul<Length<f64>> for f64 {
    type Output = Length<f64>;
    fn mul(self, rhs: Length<f64>) -> Self::Output {
        Self::Output {
            value: self * rhs.value,
        }
    }
}

impl Mul<Length<f32>> for f32 {
    type Output = Length<f32>;
    fn mul(self, rhs: Length<f32>) -> Self::Output {
        Self::Output {
            value: self * rhs.value,
        }
    }
}

impl<S: MulAssign> MulAssign<S> for Length<S> {
    fn mul_assign(&mut self, rhs: S) {
        self.value *= rhs;
    }
}

impl<S: Div<Output = S>> Div<Length<S>> for Length<S> {
    type Output = S;
    fn div(self, rhs: Length<S>) -> Self::Output {
        self.value / rhs.value
    }
}

impl<S: Div<Output = S>> Div<S> for Length<S> {
    type Output = Length<S>;
    fn div(self, rhs: S) -> Self::Output {
        Self {
            value: self.value / rhs,
        }
    }
}

impl<S: DivAssign> DivAssign<S> for Length<S> {
    fn div_assign(&mut self, rhs: S) {
        self.value /= rhs;
    }
}

impl<'a, S: Add<&'a S, Output = S>> Add<&'a Length<S>> for Length<S> {
    type Output = Length<S>;
    fn add(self, rhs: &'a Length<S>) -> Self::Output {
        Self {
            value: self.value + &rhs.value,
        }
    }
}

impl<'a, S: AddAssign<&'a S>> AddAssign<&'a Length<S>> for Length<S> {
    fn add_assign(&mut self, rhs: &'a Length<S>) {
        self.value += &rhs.value;
    }
}

impl<'a, S: Sub<&'a S, Output = S>> Sub<&'a Length<S>> for Length<S> {
    type Output = Length<S>;
    fn sub(self, rhs: &'a Length<S>) -> Self::Output {
        Self {
            value: self.value - &rhs.value,
        }
    }
}

impl<'a, S: SubAssign<&'a S>> SubAssign<&'a Length<S>> for Length<S> {
    fn sub_assign(&mut self, rhs: &'a Length<S>) {
        self.value -= &rhs.value;
    }
}

impl<'a, S: Mul<&'a S, Output = S>> Mul<&'a S> for Length<S> {
    type Output = Length<S>;
    fn mul(self, rhs: &'a S) -> Self::Output {
        Self {
            value: self.value * rhs,
        }
    }
}

impl<'a> Mul<&'a Length<f64>> for f64 {
    type Output = Length<f64>;
    fn mul(self, rhs: &'a Length<f64>) -> Self::Output {
        Self::Output {
            value: self * rhs.value,
        }
    }
}

impl<'a> Mul<&'a Length<f32>> for f32 {
    type Output = Length<f32>;
    fn mul(self, rhs: &'a Length<f32>) -> Self::Output {
        Self::Output {
            value: self * rhs.value,
        }
    }
}

impl<'a, S: MulAssign<&'a S>> MulAssign<&'a S> for Length<S> {
    fn mul_assign(&mut self, rhs: &'a S) {
        self.value *= rhs;
    }
}

impl<'a, S: Div<&'a S, Output = S>> Div<&'a Length<S>> for Length<S> {
    type Output = S;
    fn div(self, rhs: &'a Length<S>) -> Self::Output {
        self.value / &rhs.value
    }
}

impl<'a, S: Div<&'a S, Output = S>> Div<&'a S> for Length<S> {
    type Output = Length<S>;
    fn div(self, rhs: &'a S) -> Self::Output {
        Self {
            value: self.value / rhs,
        }
    }
}

impl<'a, S: DivAssign<&'a S>> DivAssign<&'a S> for Length<S> {
    fn div_assign(&mut self, rhs: &'a S) {
        self.value /= rhs;
    }
}

impl<S: PartialEq> PartialEq<Length<S>> for Length<S> {
    fn eq(&self, other: &Length<S>) -> bool {
        self.value == other.value
    }
}

impl<S: PartialOrd> PartialOrd<Length<S>> for Length<S> {
    fn partial_cmp(&self, other: &Length<S>) -> Option<std::cmp::Ordering> {
        self.value.partial_cmp(&other.value)
    }
}

impl<S: Num> Zero for Length<S> {
    fn zero() -> Self {
        Length { value: S::zero() }
    }
    fn is_zero(&self) -> bool {
        self.value.is_zero()
    }
}

pub trait Unit {
    const CONVERSION_FACTOR: f64;
}

#[derive(Debug, Clone, Copy)]
pub struct Nanometer;
impl Unit for Nanometer {
    const CONVERSION_FACTOR: f64 = 1e-3;
}

#[derive(Debug, Clone, Copy)]
pub struct Micrometer;
impl Unit for Micrometer {
    const CONVERSION_FACTOR: f64 = 1e0;
}

#[derive(Debug, Clone, Copy)]
pub struct Millimeter;
impl Unit for Millimeter {
    const CONVERSION_FACTOR: f64 = 1e3;
}

#[derive(Debug, Clone, Copy)]
pub struct Centimeter;
impl Unit for Centimeter {
    const CONVERSION_FACTOR: f64 = 1e4;
}

#[derive(Debug, Clone, Copy)]
pub struct Meter;
impl Unit for Meter {
    const CONVERSION_FACTOR: f64 = 1e6;
}

#[derive(Debug, Clone, Copy, PartialEq, PartialOrd, Eq, Ord)]
pub struct Angle<S = f64>(S);
impl<S> Angle<S> {
    pub const fn to_rad(self) -> S {
        self.0
    }
    pub fn to_deg(self) -> S
    where
        S: Div<S, Output = S> + Mul<S, Output = S> + FloatConst + FromPrimitive,
    {
        self.0 / S::PI() * S::from_f64(180.).unwrap()
    }
    pub const fn from_rad(rad: S) -> Self {
        Self(rad)
    }
    pub fn from_deg(deg: S) -> Self
    where
        S: Div<S, Output = S> + Mul<S, Output = S> + FloatConst + FromPrimitive,
    {
        Self(deg / S::from_f64(180.).unwrap() * S::PI())
    }
    pub fn abs(&self) -> Self
    where
        S: Signed,
    {
        Self(self.0.abs())
    }
}

impl<S: PartialOrd> Angle<S> {
    pub fn max<'a>(&'a self, other: &'a Self) -> &'a Self
    where
        S: PartialOrd,
    {
        if self.0 > other.0 {
            self
        } else {
            other
        }
    }
    pub fn min<'a>(&'a self, other: &'a Self) -> &'a Self
    where
        S: PartialOrd,
    {
        if self.0 < other.0 {
            self
        } else {
            other
        }
    }
}

impl<S: Default> Default for Angle<S> {
    fn default() -> Self {
        Self(S::default())
    }
}

macro_rules! triangle_function {
    ($($methods:ident),* $(,)?) => {
        impl<S:Float> Angle<S>{
            $(
                pub fn $methods(self) -> S{
                    self.to_rad().$methods()
                }
            )*
        }
    };
}

triangle_function! {
    cos,sin,tan,sinh,cosh,tanh
}

impl<S: ApproxEq> ApproxEq for Angle<S> {
    type Margin = S::Margin;
    fn approx_eq<M: Into<Self::Margin>>(self, other: Self, margin: M) -> bool {
        self.0.approx_eq(other.0, margin)
    }
}

impl<S: Num + Display> Display for Angle<S> {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        write!(f, "{}rad", self.0)
    }
}

impl<S: Add<Output = S>> Add<Angle<S>> for Angle<S> {
    type Output = Angle<S>;
    fn add(self, rhs: Angle<S>) -> Self::Output {
        Self(self.0 + rhs.0)
    }
}

impl<S: Sub<Output = S>> Sub<Angle<S>> for Angle<S> {
    type Output = Angle<S>;
    fn sub(self, rhs: Angle<S>) -> Self::Output {
        Self(self.0 - rhs.0)
    }
}

impl<S: Mul<Output = S>> Mul<S> for Angle<S> {
    type Output = Angle<S>;
    fn mul(self, rhs: S) -> Self::Output {
        Angle::<S>(self.0 * rhs)
    }
}

impl<S: Div<Output = S>> Div<S> for Angle<S> {
    type Output = Angle<S>;
    fn div(self, rhs: S) -> Self::Output {
        Angle::<S>(self.0 / rhs)
    }
}

impl<S: Div<Output = S>> Div<Angle<S>> for Angle<S> {
    type Output = S;
    fn div(self, rhs: Angle<S>) -> Self::Output {
        self.0 / rhs.0
    }
}

impl<S: Neg<Output = S>> Neg for Angle<S> {
    type Output = Angle<S>;
    fn neg(self) -> Self::Output {
        Angle::<S>(self.0.neg())
    }
}

impl<S: AddAssign> AddAssign<Angle<S>> for Angle<S> {
    fn add_assign(&mut self, rhs: Angle<S>) {
        self.0 += rhs.0;
    }
}

impl<S: SubAssign> SubAssign<Angle<S>> for Angle<S> {
    fn sub_assign(&mut self, rhs: Angle<S>) {
        self.0 -= rhs.0;
    }
}

impl<S: MulAssign> MulAssign<S> for Angle<S> {
    fn mul_assign(&mut self, rhs: S) {
        self.0 *= rhs;
    }
}

impl<S: DivAssign> DivAssign<S> for Angle<S> {
    fn div_assign(&mut self, rhs: S) {
        self.0 /= rhs;
    }
}

impl<'a, S: AddAssign<&'a S>> AddAssign<&'a Angle<S>> for Angle<S> {
    fn add_assign(&mut self, rhs: &'a Angle<S>) {
        self.0 += &rhs.0;
    }
}

impl<'a, S: SubAssign<&'a S>> SubAssign<&'a Angle<S>> for Angle<S> {
    fn sub_assign(&mut self, rhs: &'a Angle<S>) {
        self.0 -= &rhs.0;
    }
}

impl<'a, S: MulAssign<&'a S>> MulAssign<&'a S> for Angle<S> {
    fn mul_assign(&mut self, rhs: &'a S) {
        self.0 *= rhs;
    }
}

impl<'a, S: DivAssign<&'a S>> DivAssign<&'a S> for Angle<S> {
    fn div_assign(&mut self, rhs: &'a S) {
        self.0 /= rhs;
    }
}

impl<S: FloatConst> FloatConst for Angle<S> {
    #![allow(non_snake_case)]
    fn E() -> Self {
        Self(S::E())
    }

    fn FRAC_1_PI() -> Self {
        Self(S::FRAC_1_PI())
    }

    fn FRAC_1_SQRT_2() -> Self {
        Self(S::FRAC_1_SQRT_2())
    }

    fn FRAC_2_PI() -> Self {
        Self(S::FRAC_2_PI())
    }

    fn FRAC_2_SQRT_PI() -> Self {
        Self(S::FRAC_2_SQRT_PI())
    }

    fn FRAC_PI_2() -> Self {
        Self(S::FRAC_PI_2())
    }

    fn FRAC_PI_3() -> Self {
        Self(S::FRAC_PI_3())
    }

    fn FRAC_PI_4() -> Self {
        Self(S::FRAC_PI_4())
    }

    fn FRAC_PI_6() -> Self {
        Self(S::FRAC_PI_6())
    }

    fn FRAC_PI_8() -> Self {
        Self(S::FRAC_PI_8())
    }

    fn LN_10() -> Self {
        Self(S::LN_10())
    }

    fn LN_2() -> Self {
        Self(S::LN_2())
    }

    fn LOG10_E() -> Self {
        Self(S::LOG10_E())
    }

    fn LOG2_E() -> Self {
        Self(S::LOG2_E())
    }

    fn PI() -> Self {
        Self(S::PI())
    }

    fn SQRT_2() -> Self {
        Self(S::SQRT_2())
    }
}

impl Angle<f64> {
    /// π/2
    pub const FRAC_PI_2: Self = Angle(core::f64::consts::FRAC_PI_2);
    /// π/3
    pub const FRAC_PI_3: Self = Angle(core::f64::consts::FRAC_PI_3);
    /// π/4
    pub const FRAC_PI_4: Self = Angle(core::f64::consts::FRAC_PI_4);
    /// π/6
    pub const FRAC_PI_6: Self = Angle(core::f64::consts::FRAC_PI_6);
    /// π/8
    pub const FRAC_PI_8: Self = Angle(core::f64::consts::FRAC_PI_8);
    /// π
    pub const PI: Self = Angle(core::f64::consts::PI);
    /// 2π
    pub const TAU: Self = Angle(core::f64::consts::TAU);

    pub fn rem_euclid(self, a: Angle) -> Angle {
        Angle(self.0.rem_euclid(a.0))
    }

    pub fn rem_2pi(self) -> Angle {
        Angle(self.0.rem_euclid(std::f64::consts::TAU))
    }
}

/*
impl Angle<f32> {
    pub const FRAC_PI_2: Self = Angle(core::f32::consts::FRAC_PI_2);
    pub const FRAC_PI_3: Self = Angle(core::f32::consts::FRAC_PI_3);
    pub const FRAC_PI_4: Self = Angle(core::f32::consts::FRAC_PI_4);
    pub const PI: Self = Angle(core::f32::consts::PI);
}
*/

impl<S: Zero> Zero for Angle<S> {
    fn zero() -> Self {
        Angle(S::zero())
    }
    fn is_zero(&self) -> bool {
        self.0.is_zero()
    }
    fn set_zero(&mut self) {
        self.0.set_zero()
    }
}

#[const_trait]
pub trait ToQuantity<F: Mul + Div + Float + FloatConst + FromPrimitive>: Sized {
    fn nanometer(self) -> Length<F>;
    fn micrometer(self) -> Length<F>;
    fn millimeter(self) -> Length<F>;
    fn centimeter(self) -> Length<F>;
    fn meter(self) -> Length<F>;
    fn deg(self) -> Angle<F>;
    fn rad(self) -> Angle<F>;
}

macro_rules! impl_from_num_to_quantity {
    ($($t:ty),+ as $f:ident) => {
        $(
        impl const ToQuantity<$f> for $t {
            fn nanometer(self) -> Length<$f> {
                Length {
                    value: Nanometer::CONVERSION_FACTOR as $f * self as $f,
                }
            }
            fn micrometer(self) -> Length<$f> {
                Length {
                    value: Micrometer::CONVERSION_FACTOR as $f * self as $f,
                }
            }
            fn millimeter(self) -> Length<$f> {
                Length {
                    value: Millimeter::CONVERSION_FACTOR as $f * self as $f,
                }
            }
            fn centimeter(self) -> Length<$f> {
                Length {
                    value: Centimeter::CONVERSION_FACTOR as $f * self as $f,
                }
            }
            fn meter(self) -> Length<$f> {
                Length {
                    value: Meter::CONVERSION_FACTOR as $f * self as $f,
                }
            }
            fn deg(self) -> Angle<$f> {
                Angle::from_rad(self as $f * std::$f::consts::PI as $f / 180. as $f)
            }
            fn rad(self) -> Angle<$f> {
                Angle::from_rad(self as $f)
            }
        })+
    };
}

impl_from_num_to_quantity!(f64, i32, u32 as f64);

/*
impl_from_num_to_quantity!(
    f64,
    f32,
    i8,
    i16,
    i32,
    i64,
    i128,
    u8,
    u16,
    u32,
    u64,
    u128 as f32
);
 */

#[cfg(test)]
mod test {
    use std::f64::consts::PI;

    use crate::units::{Angle, Length, Micrometer, Millimeter, Nanometer};

    use super::ToQuantity;

    #[test]
    fn units_operation() {
        let l1 = Length::new::<Nanometer>(1000.);
        let l2 = Length::new::<Micrometer>(1.);
        let l3 = Length::new::<Millimeter>(1.);
        assert_eq!(l1, l2);
        assert_eq!(l3, l2 * 1000.);
        assert_eq!(l3, (l1 + l2) * 500.);
        assert!(l1 < l3);
        assert_eq!(l1 / l2, 1.);
        let deg = Angle::from_deg(180.);
        let ang = Angle::from_rad(PI);
        assert_eq!(deg, ang);
        assert_eq!(deg + ang, Angle::from_deg(360.));
        assert_eq!(deg - ang, Angle::from_rad(0.));
    }

    #[test]
    fn to_quantity() {
        let nano = 1.nanometer();
        let micro = 1.micrometer();
        let deg = PI.rad();
        let rad = 180.deg();
        assert_eq!(nano * 1e3, micro);
        assert_eq!(deg, rad);
    }
}
