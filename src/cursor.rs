use std::marker::PhantomData;

use nalgebra::RealField;
use num::{Float, Zero};

use crate::{
    color::{Colour, LayerData},
    draw::{
        coordinate::{Coordinate, Point},
        curve::{groups::Group, Sweep, Taper},
        transfer::{CommonTrans, Rotate, Translate},
        CircularArc, Line, Resolution,
    },
    gds::DgirCell,
    shape::{ArcCurve, Rect},
    units::{Angle, Length},
    zero, Num, Quantity,
};

#[derive(Clone, Debug)]
pub struct Cursor<T = f64>
where
    T: Num,
{
    pub pos: Point<T>,
    pub dir: Angle<T>,
}

impl<T: Num> Default for Cursor<T> {
    fn default() -> Self {
        Self {
            pos: Point::from([Zero::zero(), Zero::zero()]),
            dir: Angle::from_rad(Zero::zero()),
        }
    }
}

pub type Assemble<S, T> = Translate<Rotate<Translate<S, T>, T>, T>;

impl<T: Num> Cursor<T> {
    pub fn new<C: Into<Point<T>>>(pos: C, dir: Angle<T>) -> Self {
        Self {
            pos: pos.into(),
            dir,
        }
    }
    pub fn with_cell<Cell: AsMut<DgirCell<Length<T>>>, C: Colour>(
        self,
        cell: Cell,
        color: C,
    ) -> CellCursor<Cell, C, T> {
        CellCursor {
            cell,
            color,
            cursor: self,
            _ph: PhantomData,
        }
    }
    pub fn mut_ref_with_cell<Cell: AsMut<DgirCell<Length<T>>>, C: Colour>(
        &mut self,
        cell: Cell,
        color: C,
    ) -> CellCursor<Cell, C, T, &mut Self> {
        CellCursor {
            cell,
            color,
            cursor: self,
            _ph: PhantomData,
        }
    }
    pub fn assemble<E>(&mut self, e: E) -> Assemble<E, T>
    where
        T: RealField + Float,
        E: CommonTrans<T> + Pos<Length<T>> + Dir<T>,
    {
        let start_pos = e.start_pos();
        let end_pos = e.end_pos();
        let start_ang = e.start_ang();
        let end_ang = e.end_ang();
        let e = e
            .translate(-start_pos[0], -start_pos[1])
            .rotate(self.dir - start_ang)
            .translate(self.pos[0], self.pos[1]);
        self.pos = self.pos + (end_pos - start_pos).rotate(self.dir - start_ang);
        self.dir = self.dir + end_ang - start_ang;
        e
    }
    pub fn assemble_rev<E>(&mut self, e: E) -> Assemble<E, T>
    where
        T: RealField + Float,
        E: CommonTrans<T> + Pos<Length<T>> + Dir<T>,
    {
        let end_pos = e.start_pos();
        let start_pos = e.end_pos();
        let start_ang = e.end_ang() + Angle::from_rad(T::pi());
        let end_ang = e.start_ang() + Angle::from_rad(T::pi());
        let e = e
            .translate(-start_pos[0], -start_pos[1])
            .rotate(self.dir - start_ang)
            .translate(self.pos[0], self.pos[1]);
        self.pos = self.pos + (end_pos - start_pos).rotate(self.dir - start_ang);
        self.dir = self.dir + end_ang - start_ang;
        e
    }
}

impl<T: Num> AsMut<Cursor<T>> for Cursor<T> {
    fn as_mut(&mut self) -> &mut Cursor<T> {
        self
    }
}

impl<T: Num> AsRef<Cursor<T>> for Cursor<T> {
    fn as_ref(&self) -> &Cursor<T> {
        self
    }
}

impl<T: Num + Float> Cursor<T> {
    pub fn rotate_whole(&mut self, ang: Angle<T>) -> &mut Self {
        self.rotate_dir(ang).rotate_pos(ang)
    }
    pub fn rotate_pos(&mut self, ang: Angle<T>) -> &mut Self {
        self.pos = self.pos.rotate(ang);
        self
    }

    pub fn translate<C: Into<Point<T>>>(&mut self, trans: C) -> &mut Self {
        self.pos = self.pos.translate(trans.into());
        self
    }

    pub fn rotate_dir(&mut self, ang: Angle<T>) -> &mut Self {
        self.dir += ang;
        self
    }
}

#[derive(Debug)]
pub struct CellCursor<
    Cell: AsMut<DgirCell<Length<T>>>,
    C: Colour,
    T: Num = f64,
    Cur: AsMut<Cursor<T>> = Cursor<T>,
> {
    pub cursor: Cur,
    cell: Cell,
    pub color: C,
    _ph: PhantomData<T>,
}

impl<Cell: AsMut<DgirCell<Length<T>>>, C: Colour + Clone, T: Num, Cur: AsMut<Cursor<T>>>
    AsMut<Cursor<T>> for CellCursor<Cell, C, T, Cur>
{
    fn as_mut(&mut self) -> &mut Cursor<T> {
        self.cursor.as_mut()
    }
}

impl<
        Cell: AsMut<DgirCell<Length<T>>>,
        C: Colour + Clone,
        T: Num,
        Cur: AsMut<Cursor<T>> + AsRef<Cursor<T>>,
    > AsRef<Cursor<T>> for CellCursor<Cell, C, T, Cur>
{
    fn as_ref(&self) -> &Cursor<T> {
        self.cursor.as_ref()
    }
}

impl<
        Cell: AsMut<DgirCell<Length<T>>>,
        C: Colour + Clone,
        T: Num,
        Cur: AsMut<Cursor<T>> + AsRef<Cursor<T>>,
    > CellCursor<Cell, C, T, Cur>
{
    pub fn dir(&self) -> Angle<T> {
        self.as_ref().dir
    }
    pub fn pos(&self) -> Coordinate<Length<T>> {
        self.as_ref().pos
    }
}

impl<C: Colour, T: Num> CellCursor<DgirCell<Length<T>>, C, T> {
    pub fn new<S: ToString>(cell_name: S, color: C) -> Self {
        Self {
            cursor: Cursor::default(),
            cell: DgirCell::new(cell_name),
            color,
            _ph: PhantomData,
        }
    }
    pub fn cursor(&self) -> Cursor<T> {
        self.cursor.clone()
    }
}

impl<Cell: AsMut<DgirCell<Length<T>>>, C: Colour, T: Num> CellCursor<Cell, C, T> {
    pub fn new_from_cell(cell: Cell, color: C) -> Self {
        Self {
            cursor: Cursor::default(),
            cell,
            color,
            _ph: PhantomData,
        }
    }
}
impl<Cell: AsMut<DgirCell<Length<T>>>, C: Colour, T: Num, Cur: AsMut<Cursor<T>>>
    CellCursor<Cell, C, T, Cur>
{
    pub fn with_assembler<W: AsRef<[Length<T>]>>(
        self,
        width: W,
        res: Resolution,
    ) -> Assembler<Cell, W, C, T, Cur> {
        Assembler {
            cell_cur: CellCursor {
                cell: self.cell,
                color: self.color,
                cursor: self.cursor,
                _ph: PhantomData,
            },
            res,
            width,
        }
    }

    pub fn mut_cell(&mut self) -> &mut DgirCell<Length<T>> {
        self.cell.as_mut()
    }

    pub fn into_cell(self) -> Cell {
        self.cell
    }

    pub fn assemble_in_rev<E>(&mut self, e: E) -> &mut Self
    where
        T: RealField + Float,
        E: CommonTrans<T> + Pos<Length<T>> + Dir<T>,
        Assemble<E, T>: crate::color::Decorated<C, Quantity = Length<T>>,
    {
        self.cell.as_mut().push(
            self.color
                .clone()
                .color(self.cursor.as_mut().assemble_rev(e)),
        );
        self
    }

    pub fn assemble_in<E>(&mut self, e: E) -> &mut Self
    where
        T: RealField + Float,
        E: CommonTrans<T> + Pos<Length<T>> + Dir<T>,
        Assemble<E, T>: crate::color::Decorated<C, Quantity = Length<T>>,
    {
        self.cell
            .as_mut()
            .push(self.color.clone().color(self.cursor.as_mut().assemble(e)));
        self
    }
}
/*
pub trait QuickDrawOps<T: Num> {
    fn extend_with<C>(&mut self, curve: C) -> &mut Self
    where
        C: Sweep<Length<f64>> + Dir<f64> + Pos<Length<f64>> + Sized + Clone,
        <<C as Sweep<Length<f64>>>::Output as IntoIterator>::IntoIter: 'static;
    fn extend_with_rev<C>(&mut self, curve: C) -> &mut Self
    where
        C: Sweep<Length<f64>> + Dir<f64> + Pos<Length<f64>> + Sized + Clone,
        <<C as Sweep<Length<f64>>>::Output as IntoIterator>::IntoIter: 'static;
    fn turn(&mut self, radius: Length<f64>, a: Angle<f64>) -> &mut Self {
        self.extend_with(
            ArcCurve::new(
                CircularArc::new_origin(radius, (Angle::from_deg(0.), a), self.res),
                self.width.clone(),
            )
            .into_group(),
        );
        self
    }
    fn extend(&mut self, len: Length<f64>) -> &mut Self {
        if !len.is_zero() {
            assert!(len.is_positive());
            self.cell_cur
                .assemble_in(Rect::from_length(len, self.width.clone()).into_group());
        } else {
            log::info!("extending 0 length");
        }
        self
    }
    fn taper(&mut self, len: Length<f64>, width: W) -> &mut Self {
        self.tapered_with(Line::new([zero(), zero()], [len, zero()]), width)
    }

    fn extend_with<C>(&mut self, curve: C) -> &mut Self
    where
        C: Sweep<Length<f64>> + Dir<f64> + Pos<Length<f64>> + Sized + Clone,
        <<C as Sweep<Length<f64>>>::Output as IntoIterator>::IntoIter: 'static,
    {
        self.cell_cur.assemble_in(Group::from(
            self.width
                .as_ref()
                .iter()
                .map(|&x| curve.clone().sweep_with_pos_dir((-x / 2., x / 2.)))
                .collect::<Vec<_>>(),
        ));
        self
    }

    fn extend_with_rev<C>(&mut self, curve: C) -> &mut Self
    where
        C: Sweep<Length<f64>> + Dir<f64> + Pos<Length<f64>> + Sized + Clone,
        <<C as Sweep<Length<f64>>>::Output as IntoIterator>::IntoIter: 'static,
    {
        self.cell_cur.assemble_in_rev(Group::from(
            self.width
                .as_ref()
                .iter()
                .map(|&x| curve.clone().sweep_with_pos_dir((-x / 2., x / 2.)))
                .collect::<Vec<_>>(),
        ));
        self
    }

    fn tapered_with<T>(&mut self, taper: T, width: W) -> &mut Self
    where
        T: Taper<Length<f64>> + Dir<f64> + Pos<Length<f64>> + Sized + Clone,
        <<T as Taper<Length<f64>>>::Output as IntoIterator>::IntoIter: 'static,
    {
        self.cell_cur.assemble_in(Group::from(
            self.width
                .as_ref()
                .iter()
                .zip(width.as_ref().iter())
                .map(|(&from, &to): (&'_ Length<f64>, &'_ Length<f64>)| {
                    taper
                        .clone()
                        .taper_with_pos_dir((-from / 2., from / 2.), (-to / 2., to / 2.))
                })
                .collect::<Vec<_>>(),
        ));
        self.width = width;
        self
    }
} */

#[derive(Debug)]
pub struct Assembler<
    Cell: AsMut<DgirCell<Length<T>>>,
    W: AsRef<[Length<T>]>,
    C: Colour = Group<LayerData>,
    T: Num = f64,
    Cur: AsMut<Cursor<T>> = Cursor<T>,
> {
    pub cell_cur: CellCursor<Cell, C, T, Cur>,
    pub width: W,
    pub res: Resolution,
}

impl<
        Cell: AsMut<DgirCell<Length<T>>>,
        W: AsRef<[Length<T>]>,
        C: Colour,
        T: Num,
        Cur: AsMut<Cursor<T>>,
    > AsMut<Cursor<T>> for Assembler<Cell, W, C, T, Cur>
{
    fn as_mut(&mut self) -> &mut Cursor<T> {
        self.cell_cur.as_mut()
    }
}

impl<
        Cell: AsMut<DgirCell<Length<T>>>,
        W: AsRef<[Length<T>]>,
        C: Colour,
        T: Num,
        Cur: AsRef<Cursor<T>> + AsMut<Cursor<T>>,
    > AsRef<Cursor<T>> for Assembler<Cell, W, C, T, Cur>
{
    fn as_ref(&self) -> &Cursor<T> {
        self.cell_cur.as_ref()
    }
}

impl<W: AsRef<[Length<T>]>, T: Num, C: Colour> Assembler<DgirCell<Length<T>>, W, C, T> {
    pub fn new<S: ToString>(cell_name: S, color: C, width: W, res: Resolution) -> Self {
        Self {
            cell_cur: CellCursor {
                cursor: Cursor::default(),
                cell: DgirCell::new(cell_name),
                color,
                _ph: PhantomData,
            },
            width,
            res,
        }
    }
}
impl<Cell: AsMut<DgirCell<Length<T>>>, W: AsRef<[Length<T>]>, T: Num>
    Assembler<Cell, W, Group<LayerData>, T>
{
    pub fn new_from_cell(cell: Cell, color: Group<LayerData>, width: W, res: Resolution) -> Self {
        Self {
            cell_cur: CellCursor {
                cursor: Cursor::default(),
                cell,
                color,
                _ph: PhantomData,
            },
            width,
            res,
        }
    }
}
impl<
        Cell: AsMut<DgirCell<Length<T>>>,
        W: AsRef<[Length<T>]>,
        C: Colour,
        T: Num,
        Cur: AsMut<Cursor<T>> + AsRef<Cursor<T>>,
    > Assembler<Cell, W, C, T, Cur>
{
    pub fn dir(&self) -> Angle<T> {
        self.as_ref().dir
    }
    pub fn pos(&self) -> Coordinate<Length<T>> {
        self.as_ref().pos
    }
}

impl<Cell: AsMut<DgirCell<Length<T>>>, W: AsRef<[Length<T>]>, C: Colour, T: Num>
    Assembler<Cell, W, C, T, Cursor<T>>
{
    pub fn cursor(&self) -> Cursor<T> {
        self.cell_cur.cursor.clone()
    }
}

impl<
        Cell: AsMut<DgirCell<Length<T>>>,
        W: AsRef<[Length<T>]>,
        C: Colour,
        T: Num,
        Cur: AsMut<Cursor<T>>,
    > Assembler<Cell, W, C, T, Cur>
{
    pub fn debugger(&mut self, color: C) -> Assembler<&mut Cell, W, C, T, &mut Cur>
    where
        W: Clone,
    {
        Assembler {
            cell_cur: CellCursor {
                cursor: &mut self.cell_cur.cursor,
                cell: &mut self.cell_cur.cell,
                color,
                _ph: PhantomData,
            },
            width: self.width.clone(),
            res: self.res,
        }
    }
    pub fn mut_cell(&mut self) -> &mut DgirCell<Length<T>> {
        self.cell_cur.mut_cell()
    }
    pub fn into_cell(self) -> Cell {
        self.cell_cur.cell
    }
    pub fn set_pos<P: Into<Coordinate<Length<T>>>>(&mut self, p: P) -> &mut Self {
        self.cell_cur.cursor.as_mut().pos = p.into();
        self
    }
    pub fn set_dir(&mut self, a: Angle<T>) -> &mut Self {
        self.cell_cur.cursor.as_mut().dir = a;
        self
    }
}

pub trait Connectable<T: Num = f64>: Dir<T> + Pos<Length<T>> + Sized + Clone {}

impl<T: Num, Q: Dir<T> + Pos<Length<T>> + Sized + Clone> Connectable<T> for Q {}

impl<
        Cell: AsMut<DgirCell<Length<f64>>>,
        W: 'static + Clone + AsRef<[Length<f64>]>,
        Cur: AsMut<Cursor<f64>>,
    > Assembler<Cell, W, Group<LayerData>, f64, Cur>
{
    pub fn turn(&mut self, radius: Length<f64>, a: Angle<f64>) -> &mut Self {
        self.cell_cur.assemble_in(
            ArcCurve::new(
                CircularArc::new_origin(radius, (Angle::from_deg(0.), a), self.res),
                self.width.clone(),
            )
            .into_group(),
        );
        self
    }
    pub fn extend(&mut self, len: Length<f64>) -> &mut Self {
        if !len.is_zero() {
            if len.is_negative() {
                #[cfg(debug_assertions)]
                {
                    log::error!("extending negative length, drawing in debug layer");
                    self.cell_cur.cursor.as_mut().dir += Angle::PI;
                    let mut layers = self.cell_cur.color.clone();
                    layers.0[0] = LayerData::new(21, 0);
                    let mut debugger: Assembler<&mut Cell, W, Group<LayerData>, f64, &mut Cur> =
                        self.debugger(layers);
                    debugger
                        .cell_cur
                        .assemble_in(Rect::from_length(-len, debugger.width.clone()).into_group());
                    self.cell_cur.cursor.as_mut().dir -= Angle::PI;
                }
                #[cfg(not(debug_assertions))]
                {
                    panic!("extending negative length")
                }
            }
            self.cell_cur
                .assemble_in(Rect::from_length(len, self.width.clone()).into_group());
        } else {
            log::info!("extending 0 length");
        }
        self
    }
    pub fn taper(&mut self, len: Length<f64>, width: W) -> &mut Self {
        self.tapered_with(Line::new([zero(), zero()], [len, zero()]), width)
    }

    pub fn taper_arc(&mut self, radius: Length<f64>, a: Angle<f64>, width: W) -> &mut Self {
        if a.is_zero() {
            log::debug!("taper extending 0 angle");
            self
        } else if radius.is_negative() {
            #[cfg(debug_assertions)]
            {
                let res = self.res;
                log::error!("taper turning negative radius, drawing in debug layer");
                self.cell_cur.cursor.as_mut().dir += Angle::PI;
                let mut layers = self.cell_cur.color.clone();
                layers.0[0] = LayerData::new(21, 0);
                let mut debugger = self.debugger(layers);

                debugger.tapered_with(
                    CircularArc::new_origin(radius, (Angle::zero(), a), res),
                    width,
                );

                self.cell_cur.cursor.as_mut().dir -= Angle::PI;
                self
            }
            #[cfg(not(debug_assertions))]
            {
                panic!("extending negative radius")
            }
        } else {
            let res = self.res;
            self.tapered_with(
                CircularArc::new_origin(radius, (Angle::zero(), a), res),
                width,
            )
        }
    }

    pub fn extend_with<C>(&mut self, curve: C) -> &mut Self
    where
        C: Connectable<f64> + Sweep<Length<f64>> + 'static,
    {
        self.cell_cur.assemble_in(Group::from(
            self.width
                .as_ref()
                .iter()
                .map(|&x| curve.clone().sweep_with_pos_dir((-x / 2., x / 2.)))
                .collect::<Vec<_>>(),
        ));
        self
    }

    pub fn extend_with_rev<C>(&mut self, curve: C) -> &mut Self
    where
        C: Connectable<f64> + Sweep<Length<f64>> + 'static,
    {
        self.cell_cur.assemble_in_rev(Group::from(
            self.width
                .as_ref()
                .iter()
                .map(|&x| curve.clone().sweep_with_pos_dir((-x / 2., x / 2.)))
                .collect::<Vec<_>>(),
        ));
        self
    }

    pub fn tapered_with<T>(&mut self, taper: T, width: W) -> &mut Self
    where
        T: Connectable<f64> + Taper<Length<f64>> + 'static,
    {
        self.cell_cur.assemble_in(Group::from(
            self.width
                .as_ref()
                .iter()
                .zip(width.as_ref().iter())
                .map(|(&from, &to): (&'_ Length<f64>, &'_ Length<f64>)| {
                    taper
                        .clone()
                        .taper_with_pos_dir((-from / 2., from / 2.), (-to / 2., to / 2.))
                })
                .collect::<Vec<_>>(),
        ));
        self.width = width;
        self
    }

    pub fn tapered_with_rev<T>(&mut self, taper: T, width: W) -> &mut Self
    where
        T: Connectable<f64> + Taper<Length<f64>> + 'static,
    {
        self.cell_cur.assemble_in_rev(Group::from(
            self.width
                .as_ref()
                .iter()
                .zip(width.as_ref().iter())
                .map(|(&from, &to): (&'_ Length<f64>, &'_ Length<f64>)| {
                    taper
                        .clone()
                        .taper_with_pos_dir((-from / 2., from / 2.), (-to / 2., to / 2.))
                })
                .collect::<Vec<_>>(),
        ));
        self.width = width;
        self
    }
}
impl<const L: usize, Cell: AsMut<DgirCell<Length<f64>>>, Cur: AsMut<Cursor<f64>>>
    Assembler<Cell, [Length<f64>; L], [LayerData; L], f64, Cur>
where
    [(); (L != 0) as usize - 1]:,
{
    pub fn turn(&mut self, radius: Length<f64>, a: Angle<f64>) -> &mut Self {
        if a.is_zero() {
            log::debug!("turning 0 angle");
            self
        } else {
            self.cell_cur.assemble_in(
                ArcCurve::new(
                    CircularArc::new_origin(radius, (Angle::from_deg(0.), a), self.res),
                    self.width,
                )
                .into_array(),
            );
            self
        }
    }
    pub fn extend(&mut self, len: Length<f64>) -> &mut Self {
        if len.is_zero() {
            log::debug!("extending 0 length");
            self
        } else if len.is_negative() {
            #[cfg(debug_assertions)]
            {
                log::error!("extending negative length, drawing in debug layer");
                self.cell_cur.cursor.as_mut().dir += Angle::PI;
                let mut layers = self.cell_cur.color;
                layers[0] = LayerData::new(21, 0);
                let mut debugger = self.debugger(layers);
                debugger
                    .cell_cur
                    .assemble_in(Rect::from_length(-len, debugger.width).into_array());
                self.cell_cur.cursor.as_mut().dir -= Angle::PI;
                self
            }
            #[cfg(not(debug_assertions))]
            {
                panic!("extending negative length")
            }
        } else {
            self.cell_cur
                .assemble_in(Rect::from_length(len, self.width).into_array());
            self
        }
    }

    pub fn taper(&mut self, len: Length<f64>, width: [Length<f64>; L]) -> &mut Self {
        if len.is_zero() {
            log::debug!("taper extending 0 length");
            self
        } else if len.is_negative() {
            #[cfg(debug_assertions)]
            {
                log::error!("taper extending negative length, drawing in debug layer");
                self.cell_cur.cursor.as_mut().dir += Angle::PI;
                let mut layers = self.cell_cur.color;
                layers[0] = LayerData::new(21, 0);
                let mut debugger = self.debugger(layers);
                debugger.tapered_with(Line::new([zero(), zero()], [len, zero()]), width);
                self.cell_cur.cursor.as_mut().dir -= Angle::PI;
                self
            }
            #[cfg(not(debug_assertions))]
            {
                panic!("extending negative length")
            }
        } else {
            self.tapered_with(Line::new([zero(), zero()], [len, zero()]), width)
        }
    }

    pub fn taper_arc(
        &mut self,
        radius: Length<f64>,
        a: Angle<f64>,
        width: [Length<f64>; L],
    ) -> &mut Self {
        if a.is_zero() {
            log::debug!("taper extending 0 angle");
            self
        } else if radius.is_negative() {
            #[cfg(debug_assertions)]
            {
                let res = self.res;
                log::error!("taper extending negative radius, drawing in debug layer");
                self.cell_cur.cursor.as_mut().dir += Angle::PI;
                let mut layers = self.cell_cur.color;
                layers[0] = LayerData::new(21, 0);
                let mut debugger = self.debugger(layers);

                debugger.tapered_with(
                    CircularArc::new_origin(radius, (Angle::zero(), a), res),
                    width,
                );

                self.cell_cur.cursor.as_mut().dir -= Angle::PI;
                self
            }
            #[cfg(not(debug_assertions))]
            {
                panic!("extending negative length")
            }
        } else {
            let res = self.res;
            self.tapered_with(
                CircularArc::new_origin(radius, (Angle::zero(), a), res),
                width,
            )
        }
    }

    pub fn extend_with<C>(&mut self, curve: C) -> &mut Self
    where
        C: Connectable<f64> + Sweep<Length<f64>> + 'static,
    {
        self.cell_cur.assemble_in(
            self.width
                .map(|x| curve.clone().sweep_with_pos_dir((-x / 2., x / 2.))),
        );
        self
    }

    pub fn extend_with_rev<C>(&mut self, curve: C) -> &mut Self
    where
        C: Connectable<f64> + Sweep<Length<f64>> + 'static,
    {
        self.cell_cur.assemble_in_rev(
            self.width
                .map(|x| curve.clone().sweep_with_pos_dir((-x / 2., x / 2.))),
        );
        self
    }
    pub fn tapered_with<T>(&mut self, taper: T, width: [Length<f64>; L]) -> &mut Self
    where
        T: Connectable<f64> + Taper<Length<f64>> + 'static,
    {
        let mut w = width.into_iter();
        self.cell_cur
            .assemble_in(self.width.map(|x| (x, w.next().unwrap())).map(
                |(from, to): (Length<f64>, Length<f64>)| {
                    taper
                        .clone()
                        .taper_with_pos_dir((-from / 2., from / 2.), (-to / 2., to / 2.))
                },
            ));
        self.width = width;
        self
    }

    pub fn tapered_with_rev<T>(&mut self, taper: T, width: [Length<f64>; L]) -> &mut Self
    where
        T: Connectable<f64> + Taper<Length<f64>> + 'static,
    {
        let mut w = width.into_iter();
        self.cell_cur
            .assemble_in_rev(self.width.map(|x| (x, w.next().unwrap())).map(
                |(to, from): (Length<f64>, Length<f64>)| {
                    taper
                        .clone()
                        .taper_with_pos_dir((-from / 2., from / 2.), (-to / 2., to / 2.))
                },
            ));
        self.width = width;
        self
    }
}

pub trait Pos<Q: Quantity> {
    fn start_pos(&self) -> Coordinate<Q>;
    fn end_pos(&self) -> Coordinate<Q>;
}

pub trait Dir<A: Num> {
    fn start_ang(&self) -> Angle<A>;
    fn end_ang(&self) -> Angle<A>;
}

#[cfg(test)]
mod tests {
    use super::*;
    use crate::{
        draw::{Resolution, APPROX_EQ_MARGIN},
        zero, MICROMETER, NANOMETER,
    };
    use float_cmp::ApproxEq;
    #[test]
    fn apply_cursor() {
        let mut c: Cursor = Cursor::new((zero(), zero()), Angle::from_deg(90f64));
        let rect = Rect::new(
            Line::new((MICROMETER, MICROMETER), (MICROMETER * 2., MICROMETER * 2.)),
            [MICROMETER / 2.],
        );
        let rect: Vec<_> = c.assemble(rect.into_group()).into_iter().collect();
        assert!(c
            .pos
            .approx_eq([zero(), MICROMETER * (2.).sqrt(),].into(), APPROX_EQ_MARGIN));
        assert!(c.dir.approx_eq(Angle::from_deg(90f64), APPROX_EQ_MARGIN));
        let expected: [Coordinate<_>; 4] = [
            [MICROMETER / 4., zero()].into(),
            [MICROMETER / 4., MICROMETER * (2.).sqrt()].into(),
            [-MICROMETER / 4., MICROMETER * (2.).sqrt()].into(),
            [-MICROMETER / 4., zero()].into(),
        ];
        assert!(rect
            .iter()
            .zip(expected.iter())
            .all(|(l, r)| { l.approx_eq(*r, APPROX_EQ_MARGIN) },));

        let radius = MICROMETER * 120.;

        let circ = ArcCurve::new(
            CircularArc::new(
                radius,
                (MICROMETER, MICROMETER),
                (Angle::from_deg(30.), Angle::from_deg(60.)),
                Resolution::MinDistance(NANOMETER * 20.),
            ),
            [MICROMETER * 2., MICROMETER * 4.],
        );
        let _circ = c.assemble(circ.into_group());
        assert!(c.pos.approx_eq(
            Point::from([zero(), MICROMETER * (2.).sqrt()])
                + Point::from([
                    -radius * (1. - Angle::from_deg(30.).cos()),
                    radius * Angle::from_deg(30.).sin()
                ]),
            APPROX_EQ_MARGIN
        ))
    }
}
