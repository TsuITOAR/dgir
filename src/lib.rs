#![feature(type_alias_impl_trait)]
#![feature(unboxed_closures)]
#![feature(fn_traits)]
#![feature(generic_const_exprs)]
#![feature(const_trait_impl)]
#![feature(const_precise_live_drops)]
#![feature(const_option)]
#![allow(incomplete_features)]
#![feature(impl_trait_in_assoc_type)]
#![feature(effects)]
#![feature(maybe_uninit_array_assume_init)]
#![feature(const_fn_floating_point_arithmetic)]

use gds21::GdsPoint;
use log::{debug, info};
use std::fmt::{Debug, Display};
use units::{Length, Unit};

pub use gds21;
pub use num;

pub mod color;
pub mod cursor;
pub mod draw;
pub mod gds;
pub mod shape;
pub mod units;

pub use crate::draw::coordinate::Coordinate;

pub trait Num:
    'static
    + Copy
    + Debug
    + Display
    + PartialOrd
    + num::traits::NumAssignRef
    + num::traits::Signed
    + num::traits::ToPrimitive
{
}

impl<T> Num for T where
    T: 'static
        + Copy
        + Debug
        + Display
        + PartialOrd
        + num::traits::NumAssignRef
        + num::traits::Signed
        + num::traits::ToPrimitive
{
}

pub trait Quantity: 'static + Clone + Debug + num::Zero + PartialEq + PartialOrd {}

impl<T> Quantity for T where T: 'static + Clone + Debug + num::Zero + PartialEq + PartialOrd {}

const MAX_POINTS_NUM: usize = 8191;

pub const NANOMETER: Length<f64> = Length {
    value: units::Nanometer::CONVERSION_FACTOR,
};

pub const MICROMETER: Length<f64> = Length {
    value: units::Micrometer::CONVERSION_FACTOR,
};

pub const MILLIMETER: Length<f64> = Length {
    value: units::Millimeter::CONVERSION_FACTOR,
};

pub const CENTIMETER: Length<f64> = Length {
    value: units::Centimeter::CONVERSION_FACTOR,
};

pub const METER: Length<f64> = Length {
    value: units::Meter::CONVERSION_FACTOR,
};

pub use units::ToQuantity;

pub fn zero<T: Num>() -> Length<T> {
    Length {
        value: num::Zero::zero(),
    }
}

fn points_num_check(points: &[GdsPoint]) -> bool {
    if points.len() > MAX_POINTS_NUM {
        debug!(
            "points number({}) exceeds limit({})",
            points.len(),
            MAX_POINTS_NUM
        );
        return false;
    }
    true
}

fn close_curve(points: &mut [GdsPoint]) -> bool {
    if points.len() >= 2 && points.first() != points.last() {
        info!(
            "curve not closed, start at ({}, {}), end at ({}, {})",
            points.first().unwrap().x,
            points.first().unwrap().y,
            points.last().unwrap().x,
            points.last().unwrap().y
        );
        false
    } else {
        true
    }
}

fn split_polygon<T: Clone + PartialEq + Debug>(mut raw: Vec<T>, max_points: usize) -> Vec<Vec<T>> {
    assert!(max_points > 3);
    if raw.is_empty() {
        return Vec::new();
    }
    if raw.len() > max_points {
        let len = if raw.first() == raw.last() {
            raw.len() - 1
        } else {
            raw.len()
        };
        debug!("auto splitting polygon");
        let mut ret = Vec::new();
        let mut temp = Vec::with_capacity(len / 2 + 2);
        temp.push(raw[len / 4].clone());
        temp.extend(raw.drain(len / 4 + 1..len * 3 / 4));
        temp.push(raw[len / 4 + 1].clone());
        temp.push(temp[0].clone());
        ret.extend(split_polygon(temp, max_points));
        ret.extend(split_polygon(raw, max_points));
        ret
    } else {
        vec![raw]
    }
}

fn split_path<T: Clone + Debug>(mut raw: Vec<T>, max_points: usize) -> Vec<Vec<T>> {
    assert!(max_points > 2);
    if raw.len() > max_points {
        let len = raw.len();
        debug!("auto splitting path");
        let mut ret = Vec::new();
        let mut temp = Vec::with_capacity(len / 2 + 1);
        temp.extend(raw.drain(0..len / 2));
        temp.push(raw[0].clone());
        ret.extend(split_path(temp, max_points));
        ret.extend(split_path(raw, max_points));
        ret
    } else {
        vec![raw]
    }
}

#[cfg(test)]
mod tests {
    use super::{split_path, split_polygon};
    #[test]
    fn auto_split_polygon() {
        let v = (0..10).collect::<Vec<_>>();
        let r = split_polygon(v, 4);
        assert_eq!(r.len(), 7);
    }
    #[test]
    fn auto_split_path() {
        let v = (0..10).collect::<Vec<_>>();
        let r = split_path(v, 3);
        assert_eq!(r.len(), 5);
    }
}
