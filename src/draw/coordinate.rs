use std::{
    fmt::{Debug, Display},
    mem,
    ops::{Add, Index, IndexMut, Mul, Sub},
};

use nalgebra::Point2;
use num::Float;

use crate::{
    units::{Angle, Length},
    Num, Quantity,
};

// #[cfg(algebra)]

#[derive(Clone, Copy, PartialEq, Eq, PartialOrd)]
#[repr(transparent)]
pub struct Coordinate<Q: Quantity>(pub(crate) Point2<Q>);

impl<Q: Quantity> Coordinate<Q> {
    pub fn x(&self) -> Q {
        self[0].clone()
    }
    pub fn y(&self) -> Q {
        self[1].clone()
    }
}

impl<Q: Display + Quantity> Display for Coordinate<Q> {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        write!(f, "[{},{}]", self[0], self[1])
    }
}

impl<Q: Quantity> Debug for Coordinate<Q> {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        write!(f, "Coordinate: [{:?},{:?}]", self[0], self[1])
    }
}

pub(crate) type Point<T> = Coordinate<Length<T>>;

impl<T: Num + Float> Point<T> {
    pub fn rotate(self, ang: Angle<T>) -> Self {
        [
            self.0[0] * ang.cos() - self.0[1] * ang.sin(),
            self.0[0] * ang.sin() + self.0[1] * ang.cos(),
        ]
        .into()
    }

    pub fn translate(self, trans: impl Into<Self>) -> Self {
        self + trans.into()
    }

    pub fn norm(self) -> Length<T> {
        Length {
            value: (self.0[0].value.powi(2) + self.0[1].value.powi(2)).sqrt(),
        }
    }
    pub fn dis_to(self, other: impl Into<Self>) -> Length<T> {
        let other = other.into();
        Length {
            value: ((self.0[0].value - other.0[0].value).powi(2)
                + (self.0[1].value - other.0[1].value).powi(2))
            .sqrt(),
        }
    }
}

impl<T: Quantity + Default> Default for Coordinate<T> {
    fn default() -> Self {
        Self([T::default(), T::default()].into())
    }
}

impl<M, T> float_cmp::ApproxEq for Point<T>
where
    M: Copy + Default + float_cmp::FloatMargin,

    T: Num,
    Length<T>: float_cmp::ApproxEq<Margin = M>,
{
    type Margin = M;
    fn approx_eq<N: Into<Self::Margin>>(self, other: Self, margin: N) -> bool {
        let margin = margin.into();
        self[0].approx_eq(other[0], margin) && self[1].approx_eq(other[1], margin)
    }
}

impl<Q: Quantity> From<Coordinate<Q>> for [Q; 2] {
    fn from(c: Coordinate<Q>) -> Self {
        <[Q; 2]>::from(c.0.coords)
    }
}

impl<Q: Quantity> From<Coordinate<Q>> for (Q, Q) {
    fn from(c: Coordinate<Q>) -> Self {
        let [a, b]: [Q; 2] = c.0.coords.into();
        (a, b)
    }
}

impl<Q: Quantity> From<[Q; 2]> for Coordinate<Q> {
    fn from(f: [Q; 2]) -> Self {
        Self(Point2::from(f))
    }
}

impl<Q: Quantity> From<Point2<Q>> for Coordinate<Q> {
    fn from(f: Point2<Q>) -> Self {
        Self(f)
    }
}

macro_rules! coord_from_tuple {
    ($t:ident$(<$($gen:ident$(:$($bound:ident)*)?),+$(,)*>)? $(,)*) => {
        impl$(<$($gen$(:$($bound)*)?),+>)? From<($t$(<$($gen),+>)?, $t$(<$($gen),+>)?)> for Coordinate<$t$(<$($gen),+>)?> {
            fn from(f: ($t$(<$($gen),+>)?, $t$(<$($gen),+>)?)) -> Self {
                Self(Point2::from([f.0, f.1]))
            }
        }
    };
}

coord_from_tuple!(f64);
coord_from_tuple!(f32);
coord_from_tuple!(Length<T: Num>);

#[cfg(test)]
mod tests {
    use super::*;
    #[test]
    fn coordinate_to_array() {
        assert_eq!([1., 2.], <[_; 2]>::from(Coordinate::from([1., 2.])))
    }
    #[test]
    fn tuple_to_coordinate() {
        assert_eq!(Coordinate::from([1f64, 2.]), Coordinate::from((1., 2.)))
    }
}

impl<T: Num> Point<T> {
    pub(crate) fn to_basic(self) -> Coordinate<T> {
        unsafe {
            debug_assert_eq!(mem::size_of::<Coordinate<T>>(), mem::size_of::<Point<T>>());
            // for now, you can't do this
            //
            // return std::mem::transmute(self);
            //
            //see also https://github.com/rust-lang/rust/issues/61956#issuecomment-503626904
            ((&self as *const Point<T>) as *const Coordinate<T>).read()
        }
    }
    pub(crate) fn from_basic(basic: Coordinate<T>) -> Self {
        unsafe {
            debug_assert_eq!(mem::size_of::<Coordinate<T>>(), mem::size_of::<Point<T>>());
            ((&basic as *const Coordinate<T>) as *const Point<T>).read()
        }
    }
}

impl<I, Q: Quantity> Index<I> for Coordinate<Q>
where
    Point2<Q>: Index<I>,
{
    type Output = <Point2<Q> as Index<I>>::Output;
    fn index(&self, index: I) -> &Self::Output {
        self.0.index(index)
    }
}

impl<I, Q: Quantity> IndexMut<I> for Coordinate<Q>
where
    Point2<Q>: IndexMut<I>,
{
    fn index_mut(&mut self, index: I) -> &mut Self::Output {
        self.0.index_mut(index)
    }
}

impl<A, Q: Quantity> Add<A> for Coordinate<Q>
where
    Point2<Q>: Add<A, Output = Point2<Q>>,
{
    type Output = Self;
    fn add(self, rhs: A) -> Self::Output {
        Self(self.0 + rhs)
    }
}

impl<Q> Add<Coordinate<Q>> for Coordinate<Q>
where
    Q: Quantity + Add<Output = Q> + Copy,
{
    type Output = Self;
    fn add(self, rhs: Coordinate<Q>) -> Self::Output {
        Self::from([self.0[0] + rhs.0[0], self.0[1] + rhs.0[1]])
    }
}

impl<S, Q: Quantity> Sub<S> for Coordinate<Q>
where
    Point2<Q>: Sub<S, Output = Point2<Q>>,
{
    type Output = Self;
    fn sub(self, rhs: S) -> Self::Output {
        Self(self.0 - rhs)
    }
}

impl<Q> Sub<Coordinate<Q>> for Coordinate<Q>
where
    Q: Quantity + Sub<Output = Q> + Copy,
{
    type Output = Self;
    fn sub(self, rhs: Coordinate<Q>) -> Self::Output {
        Self::from([self.0[0] - rhs.0[0], self.0[1] - rhs.0[1]])
    }
}
#[derive(Clone, Copy, Debug, Default, PartialEq, Eq, PartialOrd, Ord)]
pub struct MulAsScalar<M>(pub(crate) M);

impl<M> From<M> for MulAsScalar<M> {
    fn from(m: M) -> Self {
        Self(m)
    }
}

impl<M, T: Num> Mul<Point<T>> for MulAsScalar<M>
where
    M: Mul<Point2<T>, Output = Point2<T>>,
{
    type Output = Point<T>;
    fn mul(self, rhs: Point<T>) -> Self::Output {
        Coordinate::from_basic(Coordinate(self.0 * rhs.to_basic().0))
    }
}

impl<'a, M, T: Num> Mul<Point<T>> for &'a MulAsScalar<M>
where
    for<'b> &'b M: Mul<Point2<T>, Output = Point2<T>>,
{
    type Output = Point<T>;
    fn mul(self, rhs: Point<T>) -> Self::Output {
        Coordinate::from_basic(Coordinate(&self.0 * rhs.to_basic().0))
    }
}
