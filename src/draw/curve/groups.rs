use std::{
    iter::{Flatten, Fuse},
    ops::Neg,
};

use nalgebra::RealField;
use num::{traits::FloatConst, Float};

use crate::{
    cursor::{Dir, Pos},
    draw::coordinate::{Coordinate, Point},
    units::{Angle, Length},
    Num, Quantity,
};

use super::{iter::Close, Area, Bias, Curve, IntoCurve};

#[derive(Clone, Copy, Debug, Default, PartialEq, Eq, PartialOrd, Ord)]
pub struct Chain<T1, T2>(pub T1, pub T2);

impl<Q, T1, T2> From<(T1, T2)> for Chain<T1, T2>
where
    T1: IntoIterator<Item = Coordinate<Q>>,
    T2: IntoIterator<Item = Coordinate<Q>>,
    Q: Quantity,
{
    fn from(value: (T1, T2)) -> Self {
        Self(value.0, value.1)
    }
}

impl<T: Float + Num + RealField, T1, T2> Chain<T1, T2>
where
    T1: IntoIterator<Item = Point<T>> + Dir<T> + Pos<Length<T>>,
    T2: IntoIterator<Item = Point<T>> + Dir<T> + Pos<Length<T>>,
{
    pub fn chain<B: IntoIterator<Item = Point<T>>>(self, other: B) -> Chain<Self, B> {
        Chain::from((self, other))
    }
}

impl<T: Float + Num, T1, T2> IntoIterator for Chain<T1, T2>
where
    T1: IntoIterator<Item = Point<T>> + Dir<T> + Pos<Length<T>>,
    T2: IntoIterator<Item = Point<T>> + Dir<T> + Pos<Length<T>>,
{
    type IntoIter =
        std::iter::Chain<T1::IntoIter, std::iter::Map<T2::IntoIter, impl Fn(Point<T>) -> Point<T>>>;
    type Item = Point<T>;
    fn into_iter(self) -> Self::IntoIter {
        let rot = self.1.start_ang() - self.0.end_ang();
        let shift = self.1.start_pos() - self.0.end_pos();
        let curve: Curve<T2::IntoIter> = self.1.into_curve();
        let f = move |p: Point<T>| (p - shift).rotate(-rot);
        self.0.into_iter().chain(curve.into_iter().map(f))
    }
}

impl<Q: Quantity, T1, T2> Bias<Q> for Chain<T1, T2>
where
    T1: Bias<Q>,
    T2: Bias<Q>,
{
    fn bias(&mut self, b: Q) {
        self.0.bias(b.clone());
        self.1.bias(b);
    }
}

impl<T: Float + Num + RealField, T1, T2> Pos<Length<T>> for Chain<T1, T2>
where
    T1: Pos<Length<T>> + Dir<T>,
    T2: Pos<Length<T>> + Dir<T>,
{
    fn start_pos(&self) -> Coordinate<Length<T>> {
        self.0.start_pos()
    }

    fn end_pos(&self) -> Coordinate<Length<T>> {
        let rot = self.1.start_ang() - self.0.end_ang();
        self.0.end_pos() + (self.1.end_pos() - self.1.start_pos()).rotate(-rot)
    }
}

impl<T: Num, T1, T2> Dir<T> for Chain<T1, T2>
where
    T1: Dir<T>,
    T2: Dir<T>,
{
    fn start_ang(&self) -> crate::units::Angle<T> {
        self.0.start_ang()
    }

    fn end_ang(&self) -> crate::units::Angle<T> {
        self.1.end_ang() - (self.1.start_ang() - self.0.end_ang())
    }
}

#[derive(Clone, Copy, Debug, Default, PartialEq, Eq, PartialOrd, Ord)]
pub struct Compound<T1, T2>(pub T1, pub T2);

impl<Q, T1, T2> Compound<T1, T2>
where
    T1: IntoIterator<Item = Coordinate<Q>>,
    T2: IntoIterator<Item = Coordinate<Q>>,
    Q: Quantity,
{
    pub fn fusion(self) -> impl Iterator<Item = Coordinate<Q>> {
        self.into_iter()
    }

    pub fn compound_with<B: IntoIterator<Item = Coordinate<Q>>>(
        self,
        other: B,
    ) -> Compound<Self, B> {
        Compound::from((self, other))
    }
}

#[derive(Clone, Debug, Default, PartialEq, Eq, PartialOrd, Ord)]
pub struct Group<T>(pub Vec<T>);

impl<Q, T> Group<T>
where
    T: IntoIterator<Item = Coordinate<Q>>,
    Q: Quantity,
{
    pub fn fusion(self) -> impl Iterator<Item = Coordinate<Q>> {
        self.into_iter()
    }

    pub fn compound_with<B: IntoIterator<Item = Coordinate<Q>>>(
        self,
        other: B,
    ) -> Compound<Self, B> {
        Compound::from((self, other))
    }
}

impl<T1, T2> From<(T1, T2)> for Compound<T1, T2> {
    fn from(f: (T1, T2)) -> Self {
        Self(f.0, f.1)
    }
}

impl<T, const LEN: usize> From<[T; LEN]> for Group<T> {
    fn from(f: [T; LEN]) -> Self {
        Self(Vec::from(f))
    }
}

impl<T> From<Vec<T>> for Group<T> {
    fn from(f: Vec<T>) -> Self {
        Self(f)
    }
}

impl<T> From<(T, T)> for Group<T> {
    fn from(f: (T, T)) -> Self {
        Self(Vec::from([f.0, f.1]))
    }
}

impl<T1, T2, Q> IntoIterator for Compound<T1, T2>
where
    Q: Quantity,
    T1: IntoIterator<Item = Coordinate<Q>>,
    T2: IntoIterator<Item = Coordinate<Q>>,
{
    type IntoIter = Close<Q, Fuse<std::iter::Chain<T1::IntoIter, T2::IntoIter>>>;
    type Item = Coordinate<Q>;
    fn into_iter(self) -> Self::IntoIter {
        Close::new(self.0.into_iter().chain(self.1).fuse())
    }
}

impl<Q, T> IntoIterator for Group<T>
where
    Q: Quantity,
    T: IntoIterator<Item = Coordinate<Q>>,
{
    type IntoIter = Close<Q, Fuse<Flatten<<Vec<T> as IntoIterator>::IntoIter>>>;
    type Item = Coordinate<Q>;
    fn into_iter(self) -> Self::IntoIter {
        Close::new(self.0.into_iter().flatten().fuse())
    }
}

// ! this trait not used yet
pub trait GraphIterator<Q: Quantity>: Sized {
    type GraphIter: Iterator<Item = Self::PointIter>;
    type PointIter: Iterator<Item = Coordinate<Q>>;
    fn unzip(self) -> Self::GraphIter;
}

impl<Q, A> GraphIterator<Q> for Area<A>
where
    Q: Quantity,
    Area<A>: IntoIterator<Item = Coordinate<Q>>,
{
    type GraphIter = std::iter::Once<<Self as IntoIterator>::IntoIter>;
    type PointIter = <Self as IntoIterator>::IntoIter;
    fn unzip(self) -> Self::GraphIter {
        std::iter::once(self.into_iter())
    }
}

impl<T1, T2, Q> GraphIterator<Q> for Compound<T1, T2>
where
    Q: Quantity,
    T1: GraphIterator<Q>,
    <T1 as GraphIterator<Q>>::PointIter: 'static,
    T2: GraphIterator<Q>,
    <T2 as GraphIterator<Q>>::PointIter: 'static,
{
    type GraphIter = std::iter::Chain<
        std::iter::Map<
            <T1 as GraphIterator<Q>>::GraphIter,
            fn(<T1 as GraphIterator<Q>>::PointIter) -> Self::PointIter,
        >,
        std::iter::Map<
            <T2 as GraphIterator<Q>>::GraphIter,
            fn(<T2 as GraphIterator<Q>>::PointIter) -> Self::PointIter,
        >,
    >;
    type PointIter = Box<dyn Iterator<Item = Coordinate<Q>>>;
    fn unzip(self) -> Self::GraphIter {
        self.0
            .unzip()
            .map((|i| Box::new(i) as Self::PointIter) as fn(_) -> _)
            .chain(
                self.1
                    .unzip()
                    .map((|i| Box::new(i) as Self::PointIter) as fn(_) -> _),
            )
    }
}

impl<T, Q> GraphIterator<Q> for Group<T>
where
    Q: Quantity,
    T: GraphIterator<Q>,
{
    type GraphIter = std::iter::FlatMap<std::vec::IntoIter<T>, T::GraphIter, fn(T) -> T::GraphIter>;
    type PointIter = T::PointIter;
    fn unzip(self) -> Self::GraphIter {
        self.0
            .into_iter()
            .flat_map((|x: T| x.unzip()) as fn(_) -> _)
    }
}

#[derive(Clone, Copy, Debug, Default, PartialEq, Eq, PartialOrd, Ord)]
pub struct Reverse<T> {
    i: T,
}

impl<T> AsRef<T> for Reverse<T> {
    fn as_ref(&self) -> &T {
        &self.i
    }
}

impl<T: IntoIterator> Reverse<T>
where
    T::IntoIter: Sized + DoubleEndedIterator,
{
    pub fn new(i: T) -> Self {
        Self { i }
    }
}

impl<T: IntoIterator> IntoIterator for Reverse<T>
where
    T::IntoIter: Sized + DoubleEndedIterator,
{
    type Item = T::Item;
    type IntoIter = std::iter::Rev<T::IntoIter>;
    fn into_iter(self) -> Self::IntoIter {
        self.i.into_iter().rev()
    }
}

impl<Q: Quantity, T> Pos<Q> for Reverse<T>
where
    T: Pos<Q>,
{
    fn start_pos(&self) -> Coordinate<Q> {
        self.i.end_pos()
    }

    fn end_pos(&self) -> Coordinate<Q> {
        self.i.start_pos()
    }
}

impl<N: Num + FloatConst, T> Dir<N> for Reverse<T>
where
    T: Dir<N>,
{
    fn start_ang(&self) -> crate::units::Angle<N> {
        self.i.end_ang() + Angle::from_rad(N::PI())
    }

    fn end_ang(&self) -> crate::units::Angle<N> {
        self.i.start_ang() + Angle::from_rad(N::PI())
    }
}

impl<Q: Quantity + Neg<Output = Q>, T> Bias<Q> for Reverse<T>
where
    T: Bias<Q>,
{
    fn bias(&mut self, b: Q) {
        self.i.bias(-b)
    }
}
