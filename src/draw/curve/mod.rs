use std::ops::Mul;

use num::ToPrimitive;

use crate::{
    cursor::{Dir, Pos},
    shape::CurvePointsIter,
    Num, Quantity,
};

use self::groups::{Compound, Group};

use super::{coordinate::Coordinate, CircularArc, Resolution};

#[cfg(feature = "rayon")]
pub mod par_iter;

pub mod groups;
pub mod iter;

#[derive(Clone, Copy, Debug, Default, PartialEq, Eq, PartialOrd, Ord)]
pub struct Curve<C> {
    pub(crate) curve: C,
}

#[derive(Clone, Copy, Debug, Default, PartialEq, Eq, PartialOrd, Ord)]
pub struct Area<A> {
    pub(crate) area: A,
}

pub trait IntoCurve {
    type Q: Quantity;
    type Curve: IntoIterator<Item = Coordinate<Self::Q>>;
    fn into_curve(self) -> Curve<Self::Curve>;
}

pub trait IntoArea {
    type Q: Quantity;
    type Area: IntoIterator<Item = Coordinate<Self::Q>>;
    fn into_area(self) -> Area<Self::Area>;
}

impl<A, Q> IntoArea for Area<A>
where
    Q: Quantity,
    A: IntoIterator<Item = Coordinate<Q>>,
{
    type Q = Q;
    type Area = A;
    fn into_area(self) -> Area<Self::Area> {
        self
    }
}

pub type SweepIterWithPosDir<Q, A, Iter> =
    CurvePointsIter<Q, A, <<Iter as Sweep<Q>>::Output as IntoIterator>::IntoIter>;

pub trait Sweep<Q: Quantity> {
    type Output: IntoIterator<Item = Coordinate<Q>>;
    fn sweep(self, range: (Q, Q)) -> Area<Self::Output>;
    fn sweep_with_pos_dir<A: Num>(self, range: (Q, Q)) -> Area<SweepIterWithPosDir<Q, A, Self>>
    where
        Self: Dir<A> + Pos<Q> + Sized,
    {
        CurvePointsIter::from_sweep(self, range)
    }
}
pub trait Bias<Q> {
    fn bias(&mut self, b: Q);
}

impl<Q, T1, T2> Bias<Q> for Compound<T1, T2>
where
    Q: Quantity,
    T1: Bias<Q>,
    T2: Bias<Q>,
{
    fn bias(&mut self, b: Q) {
        self.0.bias(b.clone());
        self.1.bias(b);
    }
}

impl<Q, T> Bias<Q> for Group<T>
where
    Q: Quantity,
    T: Bias<Q>,
{
    fn bias(&mut self, b: Q) {
        for t in self.0.iter_mut() {
            t.bias(b.clone());
        }
    }
}

pub type TaperIterWithPosDir<Q, A, Iter> =
    CurvePointsIter<Q, A, <<Iter as Taper<Q>>::Output as IntoIterator>::IntoIter>;

pub trait Taper<Q: Quantity> {
    type Output: IntoIterator<Item = Coordinate<Q>>;
    fn taper(self, from: (Q, Q), to: (Q, Q)) -> Area<Self::Output>;
    fn taper_with_pos_dir<A: Num>(
        self,
        from: (Q, Q),
        to: (Q, Q),
    ) -> Area<TaperIterWithPosDir<Q, A, Self>>
    where
        Self: Dir<A> + Pos<Q> + Sized,
    {
        CurvePointsIter::from_taper(self, from, to)
    }
}

pub trait SectionNumber {
    fn set_res_to_points_num(&mut self);
    fn sec_num(&self) -> usize;
    fn points_num(&self) -> usize {
        self.sec_num() + 1
    }
}

impl SectionNumber for CircularArc {
    fn set_res_to_points_num(&mut self) {
        match self.resolution {
            Resolution::MinNumber(_) => (),
            Resolution::MinDistance(d) => {
                let ang_range = (self.angle.1 - self.angle.0).to_rad();
                self.resolution = Resolution::MinNumber(
                    (ang_range.abs() * self.inner.radius / d)
                        .abs()
                        .to_usize()
                        .unwrap()
                        + 1,
                );
            }
        }
    }
    fn sec_num(&self) -> usize {
        let ang_range = (self.angle.1 - self.angle.0).to_rad();
        match self.resolution {
            Resolution::MinNumber(n) => {
                debug_assert!(n > 1);
                n - 1
            }
            Resolution::MinDistance(d) => (ang_range.abs() * (self.inner.radius / d).abs())
                .to_usize()
                .unwrap(),
        }
    }
}

impl<Q, T, I> Taper<Q> for T
where
    Q: Quantity + Mul<f64, Output = Q>,
    T: Clone + Bias<Q> + SectionNumber + IntoIterator<Item = Coordinate<Q>, IntoIter = I>,
    I: DoubleEndedIterator<Item = Coordinate<Q>>,
{
    type Output = impl IntoIterator<Item = Coordinate<Q>>;

    fn taper(mut self, from: (Q, Q), to: (Q, Q)) -> Area<Self::Output> {
        self.set_res_to_points_num();
        let sec_num = self.sec_num();
        let mut s1 = self.clone();
        let mut s2 = self.clone();
        s1.bias(from.0);
        s2.bias(to.0);

        let mut b1 = self.clone();
        let mut b2 = self;
        b1.bias(from.1);
        b2.bias(to.1);

        fn average_points<Q: Quantity + Mul<f64, Output = Q>>(
            p1: Coordinate<Q>,
            p2: Coordinate<Q>,
            frac: f64,
        ) -> Coordinate<Q> {
            Coordinate::from([
                p1[0].clone() * (1. - frac) + p2[0].clone() * frac,
                p1[1].clone() * (1. - frac) + p2[1].clone() * frac,
            ])
        }
        s1.into_iter()
            .zip(s2)
            .zip(0..=sec_num)
            .map(move |((p1, p2), s)| {
                let frac = s as f64 / sec_num as f64;
                debug_assert!(frac <= 1., "{}", frac);
                average_points(p1, p2, frac)
            })
            .chain(
                b1.into_iter()
                    .rev()
                    .zip(b2.into_iter().rev())
                    .zip((0..=sec_num).rev())
                    .map(move |((p1, p2), s)| {
                        let frac = s as f64 / sec_num as f64;
                        debug_assert!(frac <= 1., "{}", frac);
                        average_points(p1, p2, frac)
                    }),
            )
            .into_curve()
            .close()
    }
}

/*
pub trait Split<P>: Sized {
    fn split(self, pos: P) -> (Self, Self);
}

pub trait SplitHalf<P>: Split<P> {
    fn split_half(self) -> (Self, Self);
}



impl<P, T1, T2> Split<P> for Compound<T1, T2>
where
    P: Clone,
    T1: Split<P>,
    T2: Split<P>,
{
    fn split(self, pos: P) -> (Self, Self) {
        let t1 = self.0.split(pos.clone());
        let t2 = self.1.split(pos);
        (Self(t1.0, t2.1), Self(t1.1, t2.0))
    }
}

impl<P, T> Split<P> for Group<T>
where
    P: Clone,
    T: Split<P>,
{
    fn split(self, pos: P) -> (Self, Self) {
        let (left, right): (Vec<_>, Vec<_>) = self
            .0
            .into_iter()
            .map(move |x| x.split(pos.clone()))
            .unzip();
        (Group(left), Group(right))
    }
}


wrapper_impl!(Curve<T>,curve,Split<P>,split(self,pos:P)->(Self,Self),impl_fn a (Curve{curve:a.0},Curve{curve:a.1}));
wrapper_impl!(Area<T>,area,Split<P>,split(self,pos:P)->(Self,Self),impl_fn a (Area{area:a.0},Area{area:a.1}));
wrapper_impl!(Curve<T>,curve,SplitHalf<P>,split_half(self)->(Self,Self),impl_fn a (Curve{curve:a.0},Curve{curve:a.1}));
wrapper_impl!(Area<T>,area,SplitHalf<P>,split_half(self)->(Self,Self),impl_fn a (Area{area:a.0},Area{area:a.1}));
 */

macro_rules! wrapper_impl {
    (
        $wrapper:ident<$wgen:ident$(:$($wbound:ident)*)?$(,)*>,
        $field:ident,
        $trait:ident$(<$($tgen:ident$(:$($tbound:ident)*)?),+$(,)*>)?,
        $fun:ident(self$(,$arg:ident:$arg_type:ty)*$(,)*)$(->$ret:ty)?
        $(,associate $asso_type:ident)*
        $(,impl_fn $temp:ident $im:expr)?
        $(,)*
    ) => {
            impl<$wgen:$trait$(<$($tgen),*>)? $(+$($wbound:ident)+*)? $(,$($tgen$(:$($tbound)*)?),+)?> $trait$(<$($tgen),*>)? for $wrapper<$wgen>
            {
                $(type $asso_type=T::$asso_type;)*
                fn $fun(self$(,$arg:$arg_type)*)$(->$ret)?{
                    wrapper_impl!(@get_impl self.$field.$fun($($arg),*)$(,$temp,$im)?)
                }
            }
        };
        (
        $wrapper:ident<$wgen:ident$(:$($wbound:ident)*)?$(,)*>,
        $field:ident,
        $trait:ident$(<$($tgen:ident$(:$($tbound:ident)*)?),+$(,)*>)?,
        $fun:ident(&mut self$(,$arg:ident:$arg_type:ty)*$(,)*)$(->$ret:ty)?
        $(,associate $asso_type:ident)*
        $(,impl_fn $temp:ident $im:expr)?
        $(,)*
    ) => {
            impl<$wgen:$trait$(<$($tgen),*>)? $(+$($wbound:ident)+*)? $(,$($tgen$(:$($tbound)*)?),+)?> $trait$(<$($tgen),*>)? for $wrapper<$wgen>
            {
                $(type $asso_type=T::$asso_type;)*
                fn $fun(&mut self$(,$arg:$arg_type)*)$(->$ret)?{
                    wrapper_impl!(@get_impl self.$field.$fun($($arg),*)$(,$temp,$im)?)
                }
            }
        };
        (
        $wrapper:ident<$wgen:ident$(:$($wbound:ident)*)?$(,)*>,
        $field:ident,
        $trait:ident$(<$($tgen:ident$(:$($tbound:ident)*)?),+$(,)*>)?,
        $fun:ident(&self$(,$arg:ident:$arg_type:ty)*$(,)*)$(->$ret:ty)?
        $(,associate $asso_type:ident)*
        $(,impl_fn $temp:ident $im:expr)?
        $(,)*
    ) => {
            impl<$wgen:$trait$(<$($tgen),*>)? $(+$($wbound:ident)+*)? $(,$($tgen$(:$($tbound)*)?),+)?> $trait$(<$($tgen),*>)? for $wrapper<$wgen>
            {
                $(type $asso_type=T::$asso_type;)*
                fn $fun(&self$(,$arg:$arg_type)*)$(->$ret)?{
                    wrapper_impl!(@get_impl self.$field.$fun($($arg),*)$(,$temp,$im)?)
                }
            }
        };
        (
        $wrapper:ident<$wgen:ident$(:$($wbound:ident)*)?$(,)*>,
        $field:ident,
        $trait:ident$(<$($tgen:ident$(:$($tbound:ident)*)?),+$(,)*>)?,
        $fun:ident($($arg:ident:$arg_type:ty),*$(,)*)$(->$ret:ty)?
        $(,associate $asso_type:ident)*
        $(,impl_fn $temp:ident $im:expr)?
        $(,)*
    ) => {
            impl<$wgen:$trait$(<$($tgen),*>)? $(+$($wbound:ident)+*)? $(,$($tgen$(:$($tbound)*)?),+)?> $trait$(<$($tgen),*>)? for $wrapper<$wgen>
            {
                $(type $asso_type=T::$asso_type;)*
                fn $fun($($arg:$arg_type,)*)$(->$ret)?{
                    wrapper_impl!(@get_impl self.$field::$fun($($arg),*)$(,$temp,$im)?)
                }
            }
        };
    (
        @get_impl $origin:expr,$temp:ident,$im:expr$(,)*
    ) =>{
            {
                let $temp=$origin;
                $im
            }
        };
    (
        @get_impl $origin:expr$(,)*
    ) =>{
            $origin
        };
}
wrapper_impl!(Curve<T>, curve, Bias<Q: Quantity>, bias(&mut self, b: Q));
wrapper_impl!(Area<T>, area, Bias<Q: Quantity>, bias(&mut self, b: Q));
