use std::mem::{self, MaybeUninit};

use num::{traits::FloatConst, Float, FromPrimitive};

use crate::{
    cursor::{Dir, Pos},
    draw::{
        coordinate::Point,
        curve::{
            groups::{Compound, Group},
            Area, Curve, Sweep, Taper,
        },
        CircularArc, Line,
    },
    units::{Angle, Length},
    zero, Coordinate, Num, Quantity,
};

impl<Q: Quantity, C: Pos<Q>> Pos<Q> for Curve<C> {
    fn start_pos(&self) -> Coordinate<Q> {
        self.curve.start_pos()
    }
    fn end_pos(&self) -> Coordinate<Q> {
        self.curve.end_pos()
    }
}

impl<Q: Quantity, A: Pos<Q>> Pos<Q> for Area<A> {
    fn start_pos(&self) -> Coordinate<Q> {
        self.area.start_pos()
    }
    fn end_pos(&self) -> Coordinate<Q> {
        self.area.end_pos()
    }
}

impl<Q: Num, C: Dir<Q>> Dir<Q> for Curve<C> {
    fn start_ang(&self) -> Angle<Q> {
        self.curve.start_ang()
    }
    fn end_ang(&self) -> Angle<Q> {
        self.curve.end_ang()
    }
}

impl<Q: Num, A: Dir<Q>> Dir<Q> for Area<A> {
    fn start_ang(&self) -> Angle<Q> {
        self.area.start_ang()
    }
    fn end_ang(&self) -> Angle<Q> {
        self.area.end_ang()
    }
}

impl<T: Num + Float + FromPrimitive + FloatConst> Pos<Length<T>> for CircularArc<T> {
    fn start_pos(&self) -> Coordinate<Length<T>> {
        (
            self.inner.radius() * self.angle.0.cos() + self.center[0],
            self.inner.radius() * self.angle.0.sin() + self.center[1],
        )
            .into()
    }
    fn end_pos(&self) -> Coordinate<Length<T>> {
        (
            self.inner.radius() * self.angle.1.to_rad().cos() + self.center[0],
            self.inner.radius() * self.angle.1.to_rad().sin() + self.center[1],
        )
            .into()
    }
}

impl<T: Num + FromPrimitive> Dir<T> for CircularArc<T> {
    fn start_ang(&self) -> Angle<T> {
        if self.angle.0 < self.angle.1 {
            self.angle.0 + Angle::from_rad(T::from_f64(std::f64::consts::FRAC_PI_2).unwrap())
        } else {
            self.angle.0 - Angle::from_rad(T::from_f64(std::f64::consts::FRAC_PI_2).unwrap())
        }
    }
    fn end_ang(&self) -> Angle<T> {
        if self.angle.0 < self.angle.1 {
            self.angle.1 + Angle::from_rad(T::from_f64(std::f64::consts::FRAC_PI_2).unwrap())
        } else {
            self.angle.1 - Angle::from_rad(T::from_f64(std::f64::consts::FRAC_PI_2).unwrap())
        }
    }
}

impl<T: Num + Float> Pos<Length<T>> for Line<T> {
    fn start_pos(&self) -> Coordinate<Length<T>> {
        self.start
    }
    fn end_pos(&self) -> Coordinate<Length<T>> {
        self.end
    }
}

impl<T: Num + Float + FloatConst> Dir<T> for Line<T> {
    fn start_ang(&self) -> Angle<T> {
        let (x, y) = (self.end - self.start).into();
        if x.value.is_zero() {
            Angle::from_rad(T::FRAC_PI_2() * y.value.signum())
        } else {
            Angle::from_rad(
                (y / x).atan()
                    + if x.value.is_positive() {
                        T::zero()
                    } else {
                        T::PI()
                    },
            )
        }
    }
    fn end_ang(&self) -> Angle<T> {
        self.start_ang()
    }
}

impl<Q: Quantity, T1, T2> Pos<Q> for Compound<T1, T2>
where
    T1: Pos<Q>,
{
    fn start_pos(&self) -> Coordinate<Q> {
        self.0.start_pos()
    }
    fn end_pos(&self) -> Coordinate<Q> {
        self.0.end_pos()
    }
}

impl<Q: Quantity, T> Pos<Q> for Group<T>
where
    T: Pos<Q>,
{
    fn start_pos(&self) -> Coordinate<Q> {
        self.0
            .first()
            .map(|x| x.start_pos())
            .unwrap_or_else(|| Coordinate::from([Q::zero(), Q::zero()]))
    }
    fn end_pos(&self) -> Coordinate<Q> {
        self.0
            .first()
            .map(|x| x.end_pos())
            .unwrap_or_else(|| Coordinate::from([Q::zero(), Q::zero()]))
    }
}

impl<Q: Num, T1, T2> Dir<Q> for Compound<T1, T2>
where
    T1: Dir<Q>,
{
    fn start_ang(&self) -> Angle<Q> {
        self.0.start_ang()
    }
    fn end_ang(&self) -> Angle<Q> {
        self.0.end_ang()
    }
}

impl<Q: Num, T> Dir<Q> for Group<T>
where
    T: Dir<Q>,
{
    fn start_ang(&self) -> Angle<Q> {
        self.0
            .first()
            .map(|x| x.start_ang())
            .unwrap_or_else(|| Angle::from_rad(Q::zero()))
    }
    fn end_ang(&self) -> Angle<Q> {
        self.0
            .first()
            .map(|x| x.end_ang())
            .unwrap_or_else(|| Angle::from_rad(Q::zero()))
    }
}

impl<const L: usize, U: Pos<Q>, Q: Quantity> Pos<Q> for [U; L]
where
    [(); (L != 0) as usize - 1]:,
{
    fn start_pos(&self) -> Coordinate<Q> {
        self.first().unwrap().start_pos()
    }

    fn end_pos(&self) -> Coordinate<Q> {
        self.first().unwrap().end_pos()
    }
}

impl<const L: usize, U: Dir<A>, A: Num> Dir<A> for [U; L]
where
    [(); (L != 0) as usize - 1]:,
{
    fn start_ang(&self) -> Angle<A> {
        self.first().unwrap().start_ang()
    }
    fn end_ang(&self) -> Angle<A> {
        self.first().unwrap().end_ang()
    }
}

#[derive(Clone, Copy, Debug)]
pub struct ArcCurve<W: AsRef<[Length<f64>]>> {
    arc: CircularArc,
    width: W,
}

impl<W: AsRef<[Length<f64>]>> ArcCurve<W> {
    pub fn new(arc: CircularArc, width: W) -> Self {
        Self { arc, width }
    }
    pub fn rev(mut self) -> Self {
        mem::swap(&mut self.arc.angle.0, &mut self.arc.angle.1);
        self
    }
    pub fn into_group(
        self,
    ) -> Group<Area<impl Pos<Length<f64>> + Dir<f64> + IntoIterator<Item = Point<f64>>>> {
        Group(
            self.width
                .as_ref()
                .iter()
                .map(|x| self.arc.sweep_with_pos_dir((-*x / 2., *x / 2.)))
                .collect(),
        )
    }
}

impl<W: AsRef<[Length<f64>]>> AsRef<CircularArc<f64>> for ArcCurve<W> {
    fn as_ref(&self) -> &CircularArc<f64> {
        &self.arc
    }
}

impl<W: AsRef<[Length<f64>]>> AsMut<CircularArc<f64>> for ArcCurve<W> {
    fn as_mut(&mut self) -> &mut CircularArc<f64> {
        &mut self.arc
    }
}

impl<const L: usize> ArcCurve<[Length<f64>; L]> {
    pub fn into_array(
        self,
    ) -> [Area<impl Pos<Length<f64>> + Dir<f64> + IntoIterator<Item = Point<f64>>>; L] {
        self.width
            .map(|x| self.arc.sweep_with_pos_dir((-x / 2., x / 2.)))
    }
}
impl<W: AsRef<[Length<f64>]>> Pos<Length<f64>> for ArcCurve<W> {
    fn start_pos(&self) -> Coordinate<Length<f64>> {
        self.arc.start_pos()
    }
    fn end_pos(&self) -> Coordinate<Length<f64>> {
        self.arc.end_pos()
    }
}

impl<W: AsRef<[Length<f64>]>> Dir<f64> for ArcCurve<W> {
    fn start_ang(&self) -> Angle<f64> {
        self.arc.start_ang()
    }
    fn end_ang(&self) -> Angle<f64> {
        self.arc.end_ang()
    }
}

#[derive(Clone, Debug)]
pub struct TaperedCirCularArc<W: AsRef<[Length<f64>]>> {
    arc: CircularArc,
    width: (W, W),
}

impl<W: AsRef<[Length<f64>]>> TaperedCirCularArc<W> {
    pub fn new(arc: CircularArc, width: (W, W)) -> Self {
        Self { arc, width }
    }
    pub fn rev(mut self) -> Self {
        mem::swap(&mut self.arc.angle.0, &mut self.arc.angle.1);
        self
    }
    pub fn into_group(
        self,
    ) -> Group<Area<impl Pos<Length<f64>> + Dir<f64> + IntoIterator<Item = Point<f64>>>> {
        Group(
            self.width
                .0
                .as_ref()
                .iter()
                .zip(self.width.1.as_ref().iter())
                .map(|(from, to)| {
                    self.arc
                        .taper_with_pos_dir((-*from / 2., *from / 2.), (-*to / 2., *to / 2.))
                })
                .collect(),
        )
    }
}

impl<const L: usize> TaperedCirCularArc<[Length<f64>; L]> {
    pub fn into_array(
        self,
    ) -> [Area<impl Pos<Length<f64>> + Dir<f64> + IntoIterator<Item = Point<f64>>>; L] {
        let mut ret = [const {
            MaybeUninit::<
                Area<crate::draw::curve::TaperIterWithPosDir<Length<f64>, f64, CircularArc>>,
            >::uninit()
        }; L];

        self.width
            .0
            .into_iter()
            .zip(self.width.1)
            .map(|(from, to)| {
                self.arc
                    .taper_with_pos_dir((-from / 2., from / 2.), (-to / 2., to / 2.))
            })
            .enumerate()
            .for_each(|(i, x)| {
                ret[i].write(x);
            });
        unsafe { MaybeUninit::array_assume_init(ret) }
    }
}

impl<W: AsRef<[Length<f64>]>> AsRef<CircularArc<f64>> for TaperedCirCularArc<W> {
    fn as_ref(&self) -> &CircularArc<f64> {
        &self.arc
    }
}

impl<W: AsRef<[Length<f64>]>> AsMut<CircularArc<f64>> for TaperedCirCularArc<W> {
    fn as_mut(&mut self) -> &mut CircularArc<f64> {
        &mut self.arc
    }
}

impl<W: AsRef<[Length<f64>]>> Pos<Length<f64>> for Rect<W> {
    fn start_pos(&self) -> Coordinate<Length<f64>> {
        self.line.start_pos()
    }
    fn end_pos(&self) -> Coordinate<Length<f64>> {
        self.line.end_pos()
    }
}

impl<W: AsRef<[Length<f64>]>> Dir<f64> for Rect<W> {
    fn start_ang(&self) -> Angle<f64> {
        self.line.start_ang()
    }
    fn end_ang(&self) -> Angle<f64> {
        self.line.end_ang()
    }
}

pub struct CurvePointsIter<Q: Quantity, A: Num, I> {
    pub(crate) pos: (Coordinate<Q>, Coordinate<Q>),
    pub(crate) ang: (Angle<A>, Angle<A>),
    pub(crate) iter: I,
}

impl<Q: Quantity, A: Num, I: Iterator<Item = Coordinate<Q>>> CurvePointsIter<Q, A, I> {
    pub fn from_sweep<T>(t: T, bias: (Q, Q)) -> Area<Self>
    where
        T: Sweep<Q> + Pos<Q> + Dir<A>,
        T::Output: IntoIterator<IntoIter = I>,
    {
        let pos = (t.start_pos(), t.end_pos());
        let ang = (t.start_ang(), t.end_ang());
        Area {
            area: Self {
                pos,
                ang,
                iter: t.sweep(bias).into_iter(),
            },
        }
    }
    pub fn from_taper<T>(t: T, from: (Q, Q), to: (Q, Q)) -> Area<Self>
    where
        T: Taper<Q> + Pos<Q> + Dir<A>,
        T::Output: IntoIterator<IntoIter = I>,
    {
        let pos = (t.start_pos(), t.end_pos());
        let ang = (t.start_ang(), t.end_ang());
        Area {
            area: Self {
                pos,
                ang,
                iter: t.taper(from, to).into_iter(),
            },
        }
    }
}

impl<Q: Quantity, A: Num, I: Iterator<Item = Coordinate<Q>>> Iterator for CurvePointsIter<Q, A, I> {
    type Item = Coordinate<Q>;
    fn next(&mut self) -> Option<Self::Item> {
        self.iter.next()
    }
}

impl<Q: Quantity, A: Num, I: Iterator<Item = Coordinate<Q>> + DoubleEndedIterator>
    DoubleEndedIterator for CurvePointsIter<Q, A, I>
{
    fn next_back(&mut self) -> Option<Self::Item> {
        self.iter.next_back()
    }
    fn nth_back(&mut self, n: usize) -> Option<Self::Item> {
        self.iter.nth_back(n)
    }
    fn rfind<P>(&mut self, predicate: P) -> Option<Self::Item>
    where
        Self: Sized,
        P: FnMut(&Self::Item) -> bool,
    {
        self.iter.rfind(predicate)
    }
    fn rfold<B, F>(self, init: B, f: F) -> B
    where
        Self: Sized,
        F: FnMut(B, Self::Item) -> B,
    {
        self.iter.rfold(init, f)
    }
}

impl<Q: Quantity, A: Num, I: Iterator<Item = Coordinate<Q>>> Pos<Q> for CurvePointsIter<Q, A, I> {
    fn start_pos(&self) -> Coordinate<Q> {
        self.pos.0.clone()
    }
    fn end_pos(&self) -> Coordinate<Q> {
        self.pos.1.clone()
    }
}

impl<Q: Quantity, A: Num, I: Iterator<Item = Coordinate<Q>>> Dir<A> for CurvePointsIter<Q, A, I> {
    fn start_ang(&self) -> Angle<A> {
        self.ang.0
    }
    fn end_ang(&self) -> Angle<A> {
        self.ang.1
    }
}

#[derive(Clone, Copy, Debug)]
pub struct Rect<W: AsRef<[Length<f64>]>> {
    line: Line,
    width: W,
}

impl<W: AsRef<[Length<f64>]>> Rect<W> {
    pub fn new(line: Line, width: W) -> Self {
        Self { line, width }
    }
    pub fn from_length(length: Length<f64>, width: W) -> Self {
        Self {
            line: Line {
                start: [zero(), zero()].into(),
                end: [length, zero()].into(),
            },
            width,
        }
    }
    pub fn into_group(
        self,
    ) -> Group<Area<impl Pos<Length<f64>> + Dir<f64> + IntoIterator<Item = Point<f64>>>> {
        Group(
            self.width
                .as_ref()
                .iter()
                .map(|x| self.line.sweep_with_pos_dir((-*x / 2., *x / 2.)))
                .collect(),
        )
    }
    pub fn line_mut(&mut self) -> &mut Line {
        &mut self.line
    }
}

impl<const L: usize> Rect<[Length<f64>; L]> {
    pub fn into_array(
        self,
    ) -> [Area<impl Pos<Length<f64>> + Dir<f64> + IntoIterator<Item = Point<f64>>>; L] {
        self.width
            .map(|x| self.line.sweep_with_pos_dir((-x / 2., x / 2.)))
    }
}
